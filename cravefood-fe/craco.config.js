const CracoLessPlugin = require('craco-less');
const aliyunTheme = require('@ant-design/aliyun-theme');
module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              ...aliyunTheme,
              '@primary-color': '#00793f',
              '@link-color': '#00793f',
              '@link-active-color': '#00793f',
              '@link-hover-color': '#00793f',
              '@link-focus-outline': '#00793f',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
