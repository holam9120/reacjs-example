import { createAction } from 'redux-actions';

export const types = {
  REGISTER: 'REGISTER',

  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  SET_TOKEN: 'SET_TOKEN',

  LOGOUT: 'LOGOUT',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',

  // == orders
  GET_ORDER: 'GET_ORDER',
  GET_ORDER_SUCCESS: 'GET_ORDER_SUCCESS',

  // == wishlist
  GET_WISHLIST: 'GET_WISHLIST',
  GET_WISHLIST_SUCCESS: 'GET_WISHLIST_SUCCESS',

  ADD_WISHLISTS: 'ADD_WISHLISTS',
  ADD_WISHLISTS_SUCCESS: 'ADD_WISHLISTS_SUCCESS',

  REMOVE_WISHLISTS: 'REMOVE_WISHLISTS',
  REMOVE_WISHLISTS_SUCCESS: 'REMOVE_WISHLISTS_SUCCESS',

  UPDATE_USER: 'UPDATE_USER',
  UPDATE_USER_SUCCESS: 'UPDATE_USER_SUCCESS',

  // == addresses
  GET_ADDRESSES: 'GET_ADDRESSES',
  GET_ADDRESSES_SUCCESS: 'GET_ADDRESSES_SUCCESS',

  ADD_ADDRESS: 'ADD_ADDRESS',
  REMOVE_ADDRESS: 'REMOVE_ADDRESS',

  ADD_ADDRESS_SUCCESS: 'ADD_ADDRESS_SUCCESS',
  REMOVE_ADDRESS_SUCCESS: 'REMOVE_ADDRESS_SUCCESS',
};

export const actionCreator = {
  register: createAction(types.REGISTER),

  login: createAction(types.LOGIN),
  loginSuccess: createAction(types.LOGIN_SUCCESS),

  setToken: createAction(types.SET_TOKEN),

  logout: createAction(types.LOGOUT),
  logoutSuccess: createAction(types.LOGOUT_SUCCESS),

  getProductWishLists: createAction(types.GET_WISHLIST),
  getProductWishListsSuccess: createAction(types.GET_WISHLIST_SUCCESS),

  addWishLists: createAction(types.ADD_WISHLISTS),
  removeWishLists: createAction(types.REMOVE_WISHLISTS),

  addWishListsSuccess: createAction(types.ADD_WISHLISTS_SUCCESS),
  removeWishListsSuccess: createAction(types.REMOVE_WISHLISTS_SUCCESS),

  updateUser: createAction(types.UPDATE_USER),
  updateUserSuccess: createAction(types.UPDATE_USER_SUCCESS),

  getAddress: createAction(types.GET_ADDRESSES),
  getAddressSuccess: createAction(types.GET_ADDRESSES_SUCCESS),

  actAddAddress: createAction(types.ADD_ADDRESS),
  // actAddAddressSuccess: createAction(types.ADD_ADDRESS_SUCCESS),

  actRemoveAddress: createAction(types.REMOVE_ADDRESS),
  // actRemoveAddressSuccess: createAction(types.REMOVE_ADDRESS_SUCCESS),

  actGetOrder: createAction(types.GET_ORDER),
  actGetOrderSuccess: createAction(types.GET_ORDER_SUCCESS),
};
