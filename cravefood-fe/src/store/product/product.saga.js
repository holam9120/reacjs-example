import { takeLatest, put, call, all } from 'redux-saga/effects';
import { getProducts, getProductBySlug } from 'api/apiRouter';

import { sagaErrorWrapper } from 'utils/error';
import { types, actionCreators } from './product.meta';

function* sagaGetProducts(action) {
  const { type, limit } = action.payload;
  const { body, httpStatus } = yield call(getProducts, {
    type,
    limit,
  });
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchProductsSuccess(body));
  }
}

function* sagaFetchProductBySlug(action) {
  const { slug } = action.payload;
  const { body, httpStatus } = yield call(getProductBySlug, slug);
  if (httpStatus === 200) {
    yield put(actionCreators.actFetchProductBySlugSuccess(body));
  }
}

// Monitoring Sagas
function* productMonitor() {
  yield all([
    takeLatest(
      types.FETCH_PRODUCT_BY_SLUG,
      sagaErrorWrapper(sagaFetchProductBySlug)
    ),
    takeLatest(types.FETCH_PRODUCT, sagaErrorWrapper(sagaGetProducts)),
  ]);
}

export default productMonitor;
