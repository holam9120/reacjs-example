import { createAction } from 'redux-actions';

export const types = {
  FETCH_PRODUCT: 'FETCH_PRODUCT',
  FETCH_PRODUCT_SUCCESS: 'FETCH_PRODUCT_SUCCESS',

  FETCH_PRODUCT_BY_SLUG: 'FETCH_PRODUCT_BY_SLUG',
  FETCH_PRODUCT_BY_SLUG_SUCCESS: 'FETCH_PRODUCT_BY_SLUG_SUCCESS',

  SEARCH_PRODUCT: 'SEARCH_PRODUCT',
  SEARCH_PRODUCT_SUCCESS: 'SEARCH_PRODUCT_SUCCESS',
};

export const actionCreators = {
  actFetchProducts: createAction(types.FETCH_PRODUCT),
  actFetchProductsSuccess: createAction(types.FETCH_PRODUCT_SUCCESS),

  actFetchProductBySlug: createAction(types.FETCH_PRODUCT_BY_SLUG),
  actFetchProductBySlugSuccess: createAction(
    types.FETCH_PRODUCT_BY_SLUG_SUCCESS
  ),
};
