import { takeLatest, put, call, all } from 'redux-saga/effects';

import {
  getCategories,
  getCategoryBySlug,
  getProductByCategorySlug,
} from 'api/apiRouter';
import { sagaErrorWrapper } from 'utils/error';

import { types, actionCreators } from './category.meta';

function* sagaFetchCategories(action) {
  const { limit } = action.payload;
  const { body, httpStatus } = yield call(getCategories, {
    limit,
  });
  if (httpStatus === 200) {
    yield put(actionCreators.getCategoriesSuccess(body));
  }
}

function* sagaFetchCategoryBySlug(action) {
  const { slug } = action.payload;
  const { body, httpStatus } = yield call(getCategoryBySlug, slug);
  if (httpStatus === 200) {
    yield put(actionCreators.getCategoryBySlugSuccess(body));
  }
}

function* sagaFetchProductByCategorySlug(action) {
  const { slug } = action.payload;
  const { body, httpStatus } = yield call(getProductByCategorySlug, slug);
  if (httpStatus === 200) {
    yield put(actionCreators.getProductByCategorySlugSuccess(body));
  }
}

// Monitoring Sagas
function* categoryMonitor() {
  yield all([
    takeLatest(types.FETCH_CATEGORIES, sagaErrorWrapper(sagaFetchCategories)),
    takeLatest(
      types.FETCH_CATEGORY_BY_SLUG,
      sagaErrorWrapper(sagaFetchCategoryBySlug)
    ),
    takeLatest(
      types.FETCH_PRODUCT_BY_CATEGORY_SLUG,
      sagaErrorWrapper(sagaFetchProductByCategorySlug)
    ),
  ]);
}

export default categoryMonitor;
