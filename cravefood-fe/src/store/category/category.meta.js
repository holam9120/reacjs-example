import { createAction } from 'redux-actions';

export const types = {
  FETCH_CATEGORIES: 'FETCH_CATEGORIES',
  FETCH_CATEGORIES_SUCCESS: 'FETCH_CATEGORIES_SUCCESS',

  FETCH_CATEGORY_BY_SLUG: 'FETCH_CATEGORY_BY_SLUG',
  FETCH_CATEGORY_BY_SLUG_SUCCESS: 'FETCH_CATEGORY_BY_SLUG_SUCCESS',

  FETCH_PRODUCT_BY_CATEGORY_SLUG: ' FETCH_PRODUCT_BY_CATEGORY_SLUG',
  FETCH_PRODUCT_BY_CATEGORY_SLUG_SUCCESS:
    ' FETCH_PRODUCT_BY_CATEGORY_SLUG_SUCCESS',
};

export const actionCreators = {
  actFetchCategories: createAction(types.FETCH_CATEGORIES),
  getCategoriesSuccess: createAction(types.FETCH_CATEGORIES_SUCCESS),

  actFetchCategoryBySlug: createAction(types.FETCH_CATEGORY_BY_SLUG),
  getCategoryBySlugSuccess: createAction(types.FETCH_CATEGORY_BY_SLUG_SUCCESS),

  actFetchProductByCategorySlug: createAction(
    types.FETCH_PRODUCT_BY_CATEGORY_SLUG
  ),
  getProductByCategorySlugSuccess: createAction(
    types.FETCH_PRODUCT_BY_CATEGORY_SLUG_SUCCESS
  ),
};
