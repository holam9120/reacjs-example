import { types } from './category.meta';
import { handleActions } from 'redux-actions';

const defaultCategoryState = {
  categories: [],
  bySlug: {},
  productsByCategorySlug: [],
};

const getCategoriesSuccess = (state, action) => ({
  ...state,
  categories: action.payload,
});

const getCategoryBySlugSuccess = (state, action) => ({
  ...state,
  bySlug: action.payload,
});

const getProductByCategorySlugSuccess = (state, action) => ({
  ...state,
  productsByCategorySlug: action.payload,
});

export default handleActions(
  {
    [types.FETCH_CATEGORIES_SUCCESS]: getCategoriesSuccess,
    [types.FETCH_CATEGORY_BY_SLUG_SUCCESS]: getCategoryBySlugSuccess,
    [types.FETCH_PRODUCT_BY_CATEGORY_SLUG_SUCCESS]: getProductByCategorySlugSuccess,
  },
  defaultCategoryState
);
