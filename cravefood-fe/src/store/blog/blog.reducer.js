import { types } from './blog.meta';
import { handleActions } from 'redux-actions';

const defaultBlogState = {
  blogs: [],
  bySlug: {},
  total: 0,
};

const getBlogsSuccess = (state, action) => ({
  ...state,
  blogs: action.payload.data,
  total: action.payload.total,
});

const getBlogBySlugSuccess = (state, action) => ({
  ...state,
  bySlug: action.payload,
});

export default handleActions(
  {
    [types.FETCH_BLOGS_SUCCESS]: getBlogsSuccess,
    [types.FETCH_BLOG_BY_SLUG_SUCCESS]: getBlogBySlugSuccess,
  },
  defaultBlogState
);
