import { createAction } from 'redux-actions';

export const types = {
  ADD_PRODUCT_TO_CART: 'ADD_PRODUCT_TO_CART',
  ADD_PRODUCT_TO_CART_SUCCESS: 'ADD_PRODUCT_TO_CART_SUCCESS',

  REMOVE_PRODUCT_FROM_CART: 'REMOVE_PRODUCT_FROM_CART',
  REMOVE_PRODUCT_FROM_CART_SUCCESS: 'REMOVE_PRODUCT_FROM_CART_SUCCESS',

  SELECT_ADDRESS: 'SELECT_ADDRESS',
  SELECT_ADDRESS_SUCCESS: 'SELECT_ADDRESS_SUCCESS',

  ADD_DELIVERY_DATE_AND_NOTE: 'ADD_DELIVERY_DATE_AND_NOTE',
  ADD_DELIVERY_DATE_AND_NOTE_SUCCESS: 'ADD_DELIVERY_DATE_AND_NOTE_SUCCESS',

  CHECKOUT: 'CHECKOUT',
  CHECKOUT_SUCCESS: 'CHECKOUT_SUCCESS',
};

export const actionCreators = {
  actAddToCart: createAction(types.ADD_PRODUCT_TO_CART),
  actAddToCartSuccess: createAction(types.ADD_PRODUCT_TO_CART_SUCCESS),

  actRemoveProductFromCart: createAction(types.REMOVE_PRODUCT_FROM_CART),
  actRemoveProductFromCartSuccess: createAction(
    types.REMOVE_PRODUCT_FROM_CART_SUCCESS
  ),

  actSelectAddress: createAction(types.SELECT_ADDRESS),
  actSelectAddressSuccess: createAction(types.SELECT_ADDRESS_SUCCESS),

  actAddDeliveryDateAndNote: createAction(types.ADD_DELIVERY_DATE_AND_NOTE),
  actAddDeliveryDateAndNoteSuccess: createAction(
    types.ADD_DELIVERY_DATE_AND_NOTE_SUCCESS
  ),

  actCheckout: createAction(types.CHECKOUT),
  actCheckoutSuccess: createAction(types.CHECKOUT_SUCCESS),
};
