import React from 'react';
import { Row, Col, Form, Card, Input, Button } from 'antd';

import HeadPhoneNumber from 'components/AccountWrapper/HeadPhoneNumber';
import 'antd/dist/antd.css';
import './style.scss';

const ContactForm = () => {
  const [form] = Form.useForm();

  return (
    <Card bordered>
      <Row className="contact-form__container" justify="space-between">
        <Col
          xs={{ span: 24 }}
          sm={{ span: 24 }}
          lg={{ span: 7 }}
          className="contact-form__left"
        >
          <img
            className="contact-form__left--image"
            src="./assets/images/qr.jpg"
            alt="qr code"
          />
        </Col>
        <Col
          xs={{ span: 24 }}
          sm={{ span: 24 }}
          lg={{ span: 17 }}
          className="contact-form__right"
        >
          <span style={{ fontSize: '2rem', margin: '2rem 0' }}>
            <strong>Công ty cổ phần CraveFood</strong>
          </span>
          <span className="content-infor">
            Hẻm 58/15 đường số 5, khu phố 2, phường Linh Trung, quận Thủ Đức
          </span>
          <span className="content-infor">
            nhathuynh25798@gmail.com -&nbsp;
            <span style={{ color: 'var(--primary)' }}>
              <a href="https://cravefood.netlify.app/">cravefood.netlify.app</a>
            </span>
          </span>
          <span className="content-infor">
            Số điện thoại liên hệ: 0343 661 688
          </span>
          <span className="content-infor">
            Copyright © 2015&nbsp;
            <span style={{ color: 'var(--secondary)' }}>CraveFood</span>
          </span>
          <span className="content-infor">© 2020 Make by Trong Nhat</span>
        </Col>
      </Row>
      <Row justify="start">
        <Form form={form} scrollToFirstError style={{ width: '100%' }}>
          <Row className="form__container">
            <Col span={24} className="form__title">
              <h2>Liên hệ</h2>
            </Col>

            <Col
              span={24}
              className="form__item"
              style={{ marginTop: '2.4rem' }}
            >
              <span>Họ và tên</span>
              <Form.Item
                name="name"
                rules={[{ required: true, message: 'Vui lòng điền họ và tên' }]}
              >
                <Input size="large" placeholder="Điền họ và tên" />
              </Form.Item>
            </Col>

            <Col span={24} className="form__item">
              <span>Email</span>
              <Form.Item
                name="email"
                rules={[
                  {
                    type: 'email',
                    message: 'Email không hợp lệ',
                  },
                  {
                    required: true,
                    message: 'Vui lòng điền email',
                  },
                ]}
              >
                <Input size="large" placeholder="Điền email" />
              </Form.Item>
            </Col>

            <Col span={24} className="form__item">
              <span>Số điện thoại</span>
              <Form.Item
                name="phone"
                rules={[
                  {
                    pattern: '^0[0-9]{9}$',
                    required: true,
                    message: 'Hãy nhập số điện thoại',
                  },
                ]}
                className="account-input"
              >
                <Input size="large" addonBefore={<HeadPhoneNumber />} />
              </Form.Item>
            </Col>

            <Col span={24} className="form__item">
              <span>Tiêu đề</span>
              <Form.Item
                name="title"
                rules={[{ required: true, message: 'Vui lòng điền tiêu đề' }]}
              >
                <Input size="large" placeholder="Điền tiêu đề" />
              </Form.Item>
            </Col>

            <Col span={24} className="form__item">
              <span>Nội dung</span>
              <Form.Item
                name="content"
                rules={[{ required: true, message: 'Vui lòng điền nội dung' }]}
              >
                <Input.TextArea style={{ height: '15rem' }} />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Button
                htmlType="submit"
                type="primary"
                size="large"
                className="button-form"
                style={{ width: '100%' }}
              >
                Gửi thông tin
              </Button>
            </Col>
          </Row>
        </Form>
      </Row>
    </Card>
  );
};
export default ContactForm;
