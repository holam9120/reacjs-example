import React from 'react';
import { Col, Row } from 'antd';

import ProductList from 'components/ProductList';
import '../style.scss';

const CategoriesList = (props) => {
  const { categories } = props;

  return (
    <Row>
      {categories.map((item) => (
        <Col xs={24} key={item?.id}>
          <ProductList
            data={item?.products}
            title={item?.name}
            slug={item?.slug}
            slidesToShow={4}
          />
        </Col>
      ))}
    </Row>
  );
};

export default CategoriesList;
