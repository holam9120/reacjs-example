import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreators } from 'store/category/category.meta';
import ProductItem from 'components/ProductItem';

const CategorySingle = (props) => {
  const dispatch = useDispatch();
  const { location } = props;

  const fetchCategoriesBySlug = () => {
    dispatch(
      actionCreators.actFetchProductByCategorySlug({
        slug: location,
      })
    );
  };

  const { productsByCategorySlug = [] } = useSelector(
    (store) => store.category
  );

  useEffect(() => {
    fetchCategoriesBySlug();
  }, [location]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Row justify="start">
      {productsByCategorySlug?.map((it) => (
        <Col xs={12} sm={3} md={6} key={it?.id}>
          <ProductItem data={it} />
        </Col>
      ))}
    </Row>
  );
};

export default CategorySingle;
