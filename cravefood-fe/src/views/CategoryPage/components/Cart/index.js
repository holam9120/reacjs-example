import React from 'react';
import { Row, Col, Badge } from 'antd';
import { useSelector } from 'react-redux';

import CartContainer from 'components/CartContainer';

import './style.scss';

const CategoryCart = () => {
  const { total } = useSelector((store) => store.cart);

  return (
    <Row
      style={{
        position: 'relative',
        backgroundColor: 'white',
        borderRadius: 4,
        padding: 12,
      }}
    >
      <Col
        style={{
          position: 'absolute',
          top: '-4rem',
          right: '2rem',
          width: '8rem',
          height: '8rem',
          borderRadius: '50%',
          border: '12px solid white',
          backgroundColor: '#EB7100',
        }}
      >
        <Badge
          count={total}
          offset={[-10, 10]}
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              backgroundColor: '#EB7100',
              width: 60,
              height: 60,
              borderRadius: 50,
              zIndex: 4,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <img
              src="/assets/images/cart.png"
              alt="img-cart"
              style={{ width: 32, height: 32 }}
            />
          </div>
        </Badge>
      </Col>
      <Col xs={{ span: 24 }}>
        <CartContainer />
      </Col>
    </Row>
  );
};

export default CategoryCart;
