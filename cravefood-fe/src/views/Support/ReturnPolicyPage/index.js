import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const ReturnPolicyPage = () => {
  const ReturnPolicyComponent = ComponentWrapper(ContentComponent);
  return (
    <ReturnPolicyComponent
      data={menuItem?.menuItemSupport}
      title="Chính sách đổi trả"
    />
  );
};
export default ReturnPolicyPage;
