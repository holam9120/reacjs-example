import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const TermOfUsePage = () => {
  const TermOfUseComponent = ComponentWrapper(ContentComponent);
  return (
    <TermOfUseComponent
      data={menuItem?.menuItemSupport}
      title="Điều khoản sử dụng"
    />
  );
};
export default TermOfUsePage;
