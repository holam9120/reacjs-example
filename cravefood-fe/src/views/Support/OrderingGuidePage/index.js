import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const OrderingGuidePage = () => {
  const OrderingGuideComponent = ComponentWrapper(ContentComponent);
  return (
    <OrderingGuideComponent
      data={menuItem?.menuItemSupport}
      title="Hướng dẫn đặt hàng"
    />
  );
};
export default OrderingGuidePage;
