import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const DeliveryTimePage = () => {
  const DeliveryTimeComponent = ComponentWrapper(ContentComponent);
  return (
    <DeliveryTimeComponent
      data={menuItem?.menuItemSupport}
      title="Thông tin giao hàng"
    />
  );
};
export default DeliveryTimePage;
