import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const PrivacyPolicyPage = () => {
  const PrivacyPolicyComponent = ComponentWrapper(ContentComponent);
  return (
    <PrivacyPolicyComponent
      data={menuItem?.menuItemSupport}
      title="Chính sách bảo mật"
    />
  );
};
export default PrivacyPolicyPage;
