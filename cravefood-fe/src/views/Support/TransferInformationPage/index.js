import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ContentComponent from './components/ContentComponent';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const TransferInformationPage = () => {
  const TransferInformationComponent = ComponentWrapper(ContentComponent);
  return (
    <TransferInformationComponent
      data={menuItem?.menuItemSupport}
      title="Thông tin chuyển khoản"
    />
  );
};
export default TransferInformationPage;
