import React, { memo } from 'react';
import { Row, Col } from 'antd';
import { Link } from '@reach/router';

import './style.scss';

const ServiceItem = (props) => {
  const { data } = props;

  return (
    <Row
      justify="space-between"
      align="middle"
      className="service__container"
      style={{
        flexDirection: data?.align,
      }}
    >
      <Col xs={{ span: 24 }} sm={{ span: 16 }} xl={{ span: 7 }}>
        <Row className="service__background">
          <Col xs={{ span: 24 }}>
            {!!data?.background?.logo && (
              <Row className="service__background--logo">
                <span>{data?.background?.logo}</span>
              </Row>
            )}

            <Row className="service__background--content">
              <Col
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                xl={{ span: 24 }}
                style={{ paddingLeft: '0.8rem', paddingRight: '0.8rem' }}
              >
                <Link to={data?.background?.linkTo}>
                  {data?.background?.title.map((item, index) => (
                    <span className="content-link" key={index}>
                      <strong>{item}</strong>
                    </span>
                  ))}
                </Link>
                <Row>
                  <svg
                    width="80"
                    height="25"
                    viewBox="0 0 64 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="content-arrow"
                  >
                    <g clipPath="url(#clip0)">
                      <path
                        d="M64.3535 10.3536C64.5488 10.1583 64.5488 9.84171 64.3535 9.64645L61.1716 6.46447C60.9763 6.2692 60.6597 6.2692 60.4645 6.46447C60.2692 6.65973 60.2692 6.97631 60.4645 7.17157L63.2929 10L60.4645 12.8284C60.2692 13.0237 60.2692 13.3403 60.4645 13.5355C60.6597 13.7308 60.9763 13.7308 61.1716 13.5355L64.3535 10.3536ZM0 10.5H64V9.5H0V10.5Z"
                        fill="white"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0">
                        <rect width="80" height="25" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                </Row>
              </Col>
              <Col
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                xl={{ span: 24 }}
                style={{ paddingLeft: '0.8rem', paddingRight: '0.8rem' }}
              >
                <Row className="content-description">
                  {data?.background?.description.map((item, index) => (
                    <span
                      key={index}
                      style={{
                        display: 'block',
                        width: '100%',
                        fontSize: '1.9rem',
                        margin: '0.2rem 0',
                      }}
                    >
                      <strong>{item}</strong>
                    </span>
                  ))}
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
      <Col
        xs={{ span: 0 }}
        sm={{ span: 20 }}
        md={{ span: 22 }}
        lg={{ span: 20 }}
        xl={{ span: 17 }}
      >
        <Row justify="center" align="middle">
          <Col
            xs={{ span: 24 }}
            sm={{ span: 12 }}
            xl={{ span: 24 }}
            style={{ paddingLeft: '0.8rem', paddingRight: '0.8rem' }}
          >
            <Row
              gutter={[16, 0]}
              style={{ paddingLeft: '-0.8rem', paddingRight: '-0.8rem' }}
            >
              {data?.list.map((item, index) => (
                <Col sm={{ span: 8 }} md={{ span: 8 }} key={index}>
                  <Row className="service__item">
                    <Link to={item?.linkTo} style={{ display: 'inline-flex' }}>
                      <img
                        style={{ width: '100%' }}
                        src={item?.img}
                        alt="img-service"
                      />
                      <Col xs={{ span: 24 }} className="service__item--content">
                        <h3 className="item-content__title">{item?.title}</h3>
                        {item?.description && (
                          <span
                            style={{
                              display: 'block',
                              width: '100%',
                              padding: '0 2rem',
                              fontSize: '1.2rem',
                              lineHeight: '2.25rem',
                              textTransform: 'uppercase',
                              color: 'var(--quaternary)',
                            }}
                          >
                            <strong>{item?.description}</strong>
                          </span>
                        )}
                      </Col>
                    </Link>
                  </Row>
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

const ServiceContainer = () => {
  const services = [
    {
      align: 'row',
      background: {
        logo: 'SASS',
        linkTo: '/',
        title: ['SASS', 'Giao món, dụng cụ, phục vụ, thu dọn, vệ sinh'],
        description: [
          '10 - 40 người',
          'Qúy khách đã có bàn ghế',
          'Setup theo yêu cầu',
          'Đặt trước từ 60 phút',
          'Công ty - Nhà riêng',
          'Chung cư - Nơi tập luyện',
          'Cá tính vô hạn cho gói VIP',
        ],
      },
      list: [
        {
          img: '/assets/images/backgroundimage.png',
          linkTo: '/',
          title: 'SHIP AND DECOR',
          description: 'giao món + dụng cụ + ra món + thu dọn',
        },
        {
          img: '/assets/images/backgroundimage2.png',
          linkTo: '/',
          title: 'SHIP SERVICE',
          description: 'giao món + dụng cụ + ra món + thu dọn',
        },
        {
          img: '/assets/images/backgroundimage3.png',
          linkTo: '/',
          title: 'SHIP AND DECOR',
          description: 'giao món + dụng cụ + ra món + thu dọn + 2 phục vụ',
        },
      ],
    },
    {
      align: 'row-reverse',
      background: {
        linkTo: '/',
        title: ['TIỆC TRỌN GÓI', 'LƯU ĐỘNG BẤT KỲ ĐÂU'],
        description: [
          '1 - 400 bàn',
          'Bàn tròn, Buffet, Finger food',
          'Trang trí, MC, Ban nhạc, Loa',
          'Màu sắc tự chọn',
          'Đặt trước từ 4 tiếng',
          'Cưới hỏi, Đính hôn',
          'Tân gia, Khai trương, Tất niên',
          'Tiệc giỗ, Mừng thọ...',
        ],
      },
      list: [
        {
          img: '/assets/images/backgroundimage6.png',
          linkTo: '/',
          title: 'SUPER VIP',
        },
        {
          img: '/assets/images/backgroundimage5.png',
          linkTo: '/',
          title: 'VIP',
        },
        {
          img: '/assets/images/backgroundimage4.png',
          linkTo: '/',
          title: 'STANDARD',
        },
      ],
    },
  ];

  return (
    <div>
      {services.map((item, index) => (
        <ServiceItem key={index} data={item} />
      ))}
    </div>
  );
};
export default memo(ServiceContainer);
