import React, { memo, useEffect, useState } from 'react';
import { Row, Col, Carousel } from 'antd';
import { useSelector } from 'react-redux';

import 'antd/dist/antd.css';
import './style.scss';

const Slider = () => {
  const [slider, setSlider] = useState(['./assets/images/carousel1.png']);
  require('dotenv').config();
  const { banners } = useSelector((store) => store.globalValue?.globalValue);

  const changeBanner = () => {
    const newBanner = [];
    if (Array.isArray(banners)) {
      banners.forEach((i) => {
        newBanner.push(process.env.REACT_APP_BACKEND_URL + i);
      });
    }
    return setSlider(newBanner);
  };

  useEffect(() => {
    if (banners) {
      changeBanner();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [banners]);

  return (
    <div style={{ width: '100%' }}>
      <Carousel autoplay>
        {slider.map((item, index) => (
          <Row key={index}>
            <Col xs={24}>
              <Row
                className="banner-template-item"
                style={{
                  backgroundImage: `url('${item}')`,
                }}
              ></Row>
            </Col>
          </Row>
        ))}
      </Carousel>
    </div>
  );
};

export default memo(Slider);
