import React from 'react';
import { Row, Col, Input } from 'antd';
import { useSelector } from 'react-redux';

import Slider from './components/Slider';
import FloatingCart from '../../components/FloatingCart';
import ProductList from '../../components/ProductList';
import ServiceContainer from './components/ServiceContainer';

import 'antd/dist/antd.css';
import './style.scss';

const HomePage = () => {
  const { Search } = Input;

  const { showHomePage = [] } = useSelector(
    (store) => store.globalValue.globalValue
  );

  return (
    <div style={{ marginBottom: '4rem' }}>
      <FloatingCart />
      <div className="homepage-sliders-container">
        <Row justify="space-between" align="middle" className="homepage__title">
          <Col
            xs={{ span: 24 }}
            md={{ span: 12 }}
            className="homepage__title-left"
          >
            <span className="text">GIAO HÀNG SIÊU TỐC. LÊN MÂM NÓNG GIÒN!</span>
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 12 }}
            className="homepage__title-right"
          >
            <Row className="search-box-container">
              <Search
                placeholder="Tìm món ăn"
                onSearch={(value) => console.log(value)}
                size="large"
                enterButton
              />
            </Row>
          </Col>
        </Row>
        <Slider />
        <Row>
          <Col
            xs={{ span: 22, offset: 1 }}
            md={{ span: 20, offset: 2 }}
            xxl={{ span: 18, offset: 3 }}
          >
            <div style={{ marginTop: '-13rem' }}>
              {showHomePage?.map((item) => (
                <ProductList
                  data={item?.products}
                  key={item?.id}
                  title={item?.name}
                  slug={item?.slug}
                />
              ))}
            </div>
            <ServiceContainer />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default HomePage;
