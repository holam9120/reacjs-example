import React from 'react';
import { Row, Col, Table, Avatar } from 'antd';
import { useSelector } from 'react-redux';

import { calculateSalePrice } from 'utils/helper';
import currencyFormat from 'utils/currencyFormat';
import './style.scss';
import 'antd/dist/antd.css';

const CheckoutStep2 = () => {
  require('dotenv').config();
  const { items } = useSelector((store) => store.cart);
  const { cartTotalDiscountPrice, cartTotalOriginalPrice } = useSelector(
    (store) => store.cart
  );

  const columns = [
    {
      title: 'Tên món ăn',
      dataIndex: 'name',
      key: 'name',
      render: (_, item) => (
        <Row justify="center" align="middle">
          <Col>
            <Avatar
              src={process.env.REACT_APP_BACKEND_URL + _?.images?.[0]?.content}
              size={80}
              shape="square"
              style={{ marginRight: '1rem' }}
            />
          </Col>
          <Col style={{ flex: '1 1 0%' }}>
            <span
              style={{
                textTransform: 'uppercase',
                fontSize: '2rem',
                textBreak: 'break-all',
              }}
            >
              {_?.name}
            </span>
          </Col>
        </Row>
      ),
    },
    {
      title: 'Số lượng',
      dataIndex: 'quantity',
      key: 'quantity',
      render: (_, item) => (
        <Row>
          {_?.quantity?.map(
            (it, idx) =>
              !!it?.quantity && (
                <Col xs={{ span: 24 }} key={idx}>
                  <Row justify="center">
                    <Col
                      style={{
                        display: 'flex',
                        justifyContent: 'space-around',
                        flexWrap: 'wrap',
                        flex: '1 1 0%',
                        marginLeft: '0.8rem',
                      }}
                    >
                      <Row gutter={[16, 0]}>
                        <Col>
                          <img
                            src={
                              process.env.REACT_APP_BACKEND_URL +
                              it?.dishSize?.sizeImage
                            }
                            alt="s-size"
                          />
                          <span>
                            &nbsp;
                            <strong>x{it?.quantity}</strong>
                          </span>
                        </Col>
                        {!!_?.product?.discount ? (
                          <Col>
                            <span style={{ marginRight: '0.8rem' }}>
                              {currencyFormat(
                                calculateSalePrice(
                                  item?.product?.discount,
                                  it?.quantity * it?.dishSize?.price
                                )
                              )}
                            </span>
                            <span
                              style={{
                                color: 'rgb(120, 120, 120)',
                                textDecoration: 'line-through',
                              }}
                            >
                              {currencyFormat(
                                it?.quantity * it?.dishSize?.price
                              )}
                            </span>
                          </Col>
                        ) : (
                          <Col>
                            <span style={{ marginRight: '0.8rem' }}>
                              {currencyFormat(
                                it?.quantity * it?.dishSize?.price
                              )}
                            </span>
                          </Col>
                        )}
                      </Row>
                    </Col>
                  </Row>
                </Col>
              )
          )}
        </Row>
      ),
    },
    {
      title: 'Thành tiền',
      dataIndex: 'subtotal',
      key: 'subtotal',
      render: (_, item) => (
        <Row justify="center" align="middle">
          <span>{_}</span>
        </Row>
      ),
    },
  ];

  const products = items?.map((item) => {
    return {
      key: item?.product?.id,
      name: {
        name: item?.product?.name,
        images: item?.product?.images,
      },
      quantity: item,
      subtotal: currencyFormat(
        calculateSalePrice(
          item?.product?.discount,
          item?.quantity?.reduce(
            (prev, curr) => prev + curr?.quantity * curr?.dishSize?.price,
            0
          )
        )
      ),
    };
  });

  return (
    <Row gutter={[0, 32]}>
      <Col xs={24} sm={24}>
        <Table
          columns={columns}
          className="order-detail"
          dataSource={products}
          pagination={false}
          scroll={{ x: 0, y: 500 }}
        />
      </Col>
      <Col
        xs={24}
        sm={24}
        style={{
          fontSize: '1.8rem',
        }}
      >
        <Row
          justify="space-between"
          gutter={[0, 32]}
          style={{
            padding: '2.4rem',
            backgroundImage: 'url("/assets/images/subtotal-background.png")',
            backgroundSize: 'auto 100%',
          }}
        >
          <Col xs={12} sm={12}>
            <span style={{ fontSize: '2.4rem', fontWeight: '550' }}>
              Tổng chi phí:{' '}
            </span>
          </Col>
          <Col
            xs={12}
            sm={12}
            style={{ display: 'flex', justifyContent: 'end' }}
          >
            <span
              style={{
                fontSize: '2.4rem',
                fontWeight: '550',
                color: 'rgb(120, 120, 120)',
                textDecoration: 'line-through',
                marginRight: '0.8rem',
              }}
            >
              {currencyFormat(cartTotalOriginalPrice)}
            </span>
            <span style={{ fontSize: '2.4rem', fontWeight: '550' }}>
              {currencyFormat(cartTotalDiscountPrice)}
            </span>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default CheckoutStep2;
