import React, { useEffect, useState } from 'react';
import { Row, Col, Radio, Form, Input, Button, DatePicker } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import currencyFormat from 'utils/currencyFormat';
import { actionCreators } from 'store/cart/cart.meta';

import './style.scss';

const CheckoutStep3 = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const {
    cartTotalDiscountPrice = 0,
    orderNote = '',
    deliveryDate,
  } = useSelector((store) => store.cart);

  const [disable, setDisable] = useState(true);
  const [deliveryDateState, setDeliveryDateState] = useState(deliveryDate);

  useEffect(() => {
    if (deliveryDate) {
      setDeliveryDateState(deliveryDate);
      if (deliveryDate !== moment().add(1, 'days').format('DD/MM/YYYY')) {
        setDisable(false);
      }
    } else {
      setDisable(true);
      setDeliveryDateState(moment().add(1, 'days').format('DD/MM/YYYY'));
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onFinish = (value) => {
    const { note } = value;
    dispatch(
      actionCreators.actAddDeliveryDateAndNote({
        orderNote: note,
        deliveryDate: deliveryDateState,
      })
    );
  };

  const handleChangeType = (e) => {
    e.preventDefault();
    if (e.target.value === 2) {
      setDisable(false);
      setDeliveryDateState(undefined);
    } else if (e.target.value === 1) {
      setDisable(true);
      setDeliveryDateState(moment().add(1, 'days').format('DD/MM/YYYY'));
    }
  };

  const handleChangeDatePicker = (date, dateStrings) => {
    setDeliveryDateState(dateStrings);
  };

  return (
    <Row gutter={[0, 32]}>
      <Col xs={24} sm={24}>
        <span
          style={{
            fontSize: '30px',
            fontWeight: 600,
            lineHeight: '2.25rem',
          }}
        >
          Chọn ngày giao hàng
        </span>
      </Col>

      <Col xs={24} sm={24}>
        <Row
          style={{
            borderRadius: '4px',
            backgroundColor: 'white',
          }}
        >
          <Col
            xs={24}
            sm={12}
            style={{
              padding: '40px',
              borderRight: '1px solid #ddd',
            }}
          >
            <Row style={{ marginBottom: '16px' }}>
              <Radio checked={true}>
                <span
                  style={{
                    fontSize: '1.8rem',
                    fontWeight: 500,
                    lineHeight: '1.5rem',
                  }}
                >
                  Tiền mặt (Thanh toán khi nhận hàng)
                </span>
              </Radio>
            </Row>

            <Row>
              <Form form={form} scrollToFirstError onFinish={onFinish}>
                <Row style={{ width: '100%' }} gutter={[0, 10]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Nhập ghi chú cho đơn hàng (nếu có) :
                    </span>
                    <Form.Item
                      name="note"
                      style={{ marginTop: '15px' }}
                      initialValue={orderNote}
                    >
                      <Input size="large" />
                    </Form.Item>
                  </Col>

                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Chọn ngày giao hàng
                    </span>

                    <Form.Item
                      name="deliveryDateState"
                      style={{ marginTop: '10px' }}
                      initialValue={
                        !!deliveryDate &&
                        deliveryDate !==
                          moment().add(1, 'days').format('DD/MM/YYYY')
                          ? 2
                          : 1
                      }
                    >
                      <Radio.Group onChange={handleChangeType}>
                        <Row gutter={[0, 16]}>
                          <Col span={24}>
                            <Radio value={1}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Ngày mai (
                                {`${moment()
                                  .add(1, 'days')
                                  .format('DD/MM/YYYY')}
                        `}
                              </span>
                              )
                            </Radio>
                          </Col>
                          <Col span={24}>
                            <Radio value={2}>
                              <span
                                style={{
                                  fontSize: '1.8rem',
                                  fontWeight: 500,
                                  lineHeight: '1.5rem',
                                }}
                              >
                                Tuỳ chọn ngày
                              </span>
                            </Radio>
                          </Col>
                        </Row>
                      </Radio.Group>
                    </Form.Item>
                  </Col>

                  {!disable && (
                    <Col xs={24}>
                      <span
                        style={{
                          fontSize: '1.8rem',
                          fontWeight: 500,
                          lineHeight: '1.5rem',
                        }}
                      >
                        Thời hạn chọn ngày giao đơn hàng là trong vòng 2 tuần
                        tiếp theo
                      </span>
                      <Form.Item
                        name="delivery"
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng chọn ngày nhận hàng',
                          },
                        ]}
                        initialValue={
                          deliveryDate
                            ? moment(`${deliveryDate}`, 'DD/MM/YYYY')
                            : ''
                        }
                      >
                        <DatePicker
                          size="large"
                          picker="date"
                          className="customDatePicker"
                          placeholder="Chọn ngày nhận đơn hàng"
                          format="DD/MM/YYYY"
                          showToday={false}
                          onChange={handleChangeDatePicker}
                          disabledDate={(current) =>
                            moment(current).isBefore(moment().add(1, 'days')) ||
                            moment(current).isAfter(moment().add(14, 'days'))
                          }
                        />
                      </Form.Item>
                    </Col>
                  )}

                  <Col span={24} style={{ marginTop: '2.4rem' }}>
                    <Button
                      type="primary"
                      size="large"
                      className="button-form"
                      style={{ width: '100%' }}
                      htmlType="submit"
                    >
                      Xác nhận
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Row>
          </Col>
          <Col
            xs={24}
            sm={12}
            style={{
              padding: '40px',
            }}
          >
            <Row gutter={[0, 24]}>
              <Col span={24}>
                <span
                  style={{
                    fontSize: '1.8rem',
                    fontWeight: 500,
                    lineHeight: '1.5rem',
                  }}
                >
                  Tiền thanh toán:
                </span>
              </Col>
              <Col span={24}>
                <span
                  style={{
                    fontSize: '40px',
                    fontWeight: 500,
                    color: 'var(--primary)',
                    lineHeight: '1.5rem',
                  }}
                >
                  {currencyFormat(cartTotalDiscountPrice)}
                </span>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default CheckoutStep3;
