import React, { useState } from 'react';
import { Row, Col, Button, Drawer } from 'antd';
import { useSelector } from 'react-redux';

import CartItem from 'components/CartItem';
import currencyFormat from 'utils/currencyFormat';
import './style.scss';

const CheckoutStep4 = (props) => {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(!visible);
  };

  const {
    total,
    deliveryAddress,
    deliveryDate,
    orderNote,
    cartTotalDiscountPrice,
    items,
  } = useSelector((store) => store.cart);

  const {
    name,
    detail,
    ward,
    district,
    province,
    phone,
    note,
  } = deliveryAddress;

  const ChangeCurrentButton = ({ current, title = '' }) => (
    <Row gutter={[32, 0]} align="middle">
      <Col>
        <span
          style={{
            fontSize: '30px',
            fontWeight: 600,
            lineHeight: '2.25rem',
          }}
        >
          {title}
        </span>
      </Col>
      <Col>
        <Button
          size="large"
          type="default"
          className="change-current-button "
          onClick={() => {
            props.onCurrentChange(current);
          }}
        >
          Thay đổi
        </Button>
      </Col>
    </Row>
  );

  return (
    <Row gutter={[0, 32]}>
      <Col xs={24} sm={24}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <ChangeCurrentButton current={0} title="Thông tin giao hàng" />
          </Col>
          <Col
            xs={24}
            sm={24}
            style={{
              borderRadius: '4px',
              backgroundColor: 'white',
              padding: '40px',
            }}
          >
            <Row gutter={[0, 16]}>
              <Col xs={24} sm={24}>
                <Row gutter={[0, 8]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Họ tên:
                    </span>
                  </Col>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 400,
                        lineHeight: '1.5rem',
                      }}
                    >
                      {name}
                    </span>
                  </Col>
                </Row>
              </Col>
              <Col xs={24} sm={24}>
                <Row gutter={[0, 8]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Số điện thoại:
                    </span>
                  </Col>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 400,
                        lineHeight: '1.5rem',
                      }}
                    >
                      {phone}
                    </span>
                  </Col>
                </Row>
              </Col>
              <Col xs={24} sm={24}>
                <Row gutter={[0, 16]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Địa chỉ:
                    </span>
                  </Col>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 400,
                        lineHeight: '1.5rem',
                      }}
                    >
                      {detail}, {ward}, {district}, {province}
                    </span>
                  </Col>
                </Row>
              </Col>
              {note && (
                <Col xs={24} sm={24}>
                  <Row gutter={[0, 8]}>
                    <Col span={24}>
                      <span
                        style={{
                          fontSize: '1.8rem',
                          fontWeight: 500,
                          lineHeight: '1.5rem',
                        }}
                      >
                        Ghi chú:
                      </span>
                    </Col>
                    <Col span={24}>
                      <span
                        style={{
                          fontSize: '1.8rem',
                          fontWeight: 400,
                          lineHeight: '1.5rem',
                        }}
                      >
                        {note}
                      </span>
                    </Col>
                  </Row>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </Col>

      <Col span={24}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <ChangeCurrentButton current={1} title="Chi tiết đơn hàng" />
          </Col>

          <Col
            xs={24}
            sm={24}
            style={{
              borderRadius: '4px',
              backgroundColor: 'white',
            }}
          >
            <Row gutter={[0, 16]}>
              <Col
                xs={24}
                md={12}
                style={{
                  padding: '40px',
                  borderRight: '1px solid #ddd',
                }}
              >
                <Row justify="space-between">
                  <Col span={12}>
                    <Row justify="start" gutter={[0, 8]}>
                      <Col span={24}>
                        <span
                          style={{
                            fontSize: '1.8rem',
                            fontWeight: 500,
                            lineHeight: '1.5rem',
                          }}
                        >
                          Chi phí đơn hàng
                        </span>
                      </Col>
                      <Col span={24}>
                        <span
                          style={{
                            fontSize: '1.8rem',
                            fontWeight: 400,
                            lineHeight: '1.5rem',
                          }}
                        >
                          Món ăn: {total} sản phẩm
                        </span>
                      </Col>
                      <Col span={24}>
                        <span
                          style={{
                            fontSize: '1.8rem',
                            fontWeight: 400,
                            lineHeight: '1.5rem',
                          }}
                        >
                          Chi phí vận chuyển
                        </span>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={12}>
                    <Row justify="end" gutter={[0, 8]}>
                      <Col span={24}>
                        <Row justify="end">
                          <span
                            onClick={showDrawer}
                            style={{
                              fontSize: '1.8rem',
                              fontWeight: 500,
                              lineHeight: '1.5rem',
                              color: 'var(--primary)',
                              cursor: 'pointer',
                            }}
                          >
                            Xem chi tiết đơn hàng
                          </span>
                          <Drawer
                            title="Chi tiết đơn hàng"
                            placement="right"
                            width={400}
                            closable={false}
                            onClose={showDrawer}
                            visible={visible}
                          >
                            <Row gutter={[0, 16]}>
                              <Col
                                xs={{ span: 24 }}
                                style={{
                                  padding: '1rem 1.6rem',
                                  borderRadius: '0.4rem',
                                  textAlign: 'center',
                                  fontSize: '2rem',
                                  backgroundColor: 'rgb(238, 238, 238)',
                                }}
                              >
                                {total} món lẻ
                              </Col>

                              {!!items.length &&
                                items.map((item) => (
                                  <Col key={item?.product?.id}>
                                    <CartItem item={item} showButton={false} />
                                  </Col>
                                ))}

                              <Col
                                xs={{ span: 24 }}
                                style={{
                                  marginTop: '3rem',
                                  padding: '2rem 1.6rem',
                                  borderRadius: '0.4rem',
                                  fontSize: '1.6rem',
                                  backgroundColor: 'rgb(255, 228, 204)',
                                }}
                              >
                                <Row justify="space-between" align="middle">
                                  <Col xs={{ span: 12 }}>
                                    <Row>
                                      <span
                                        style={{
                                          fontSize: '1.8rem',
                                          fontWeight: 400,
                                          lineHeight: '1.5rem',
                                        }}
                                      >
                                        Tổng giá
                                      </span>
                                    </Row>
                                  </Col>
                                  <Col xs={{ span: 12 }}>
                                    <Row justify="end">
                                      <span
                                        style={{
                                          fontSize: '1.8rem',
                                          fontWeight: 400,
                                          lineHeight: '1.5rem',
                                        }}
                                      >
                                        {currencyFormat(cartTotalDiscountPrice)}
                                      </span>
                                    </Row>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Drawer>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row justify="end">
                          <span
                            style={{
                              fontSize: '1.8rem',
                              fontWeight: 400,
                              lineHeight: '1.5rem',
                            }}
                          >
                            {currencyFormat(cartTotalDiscountPrice)}
                          </span>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row justify="end">
                          <span
                            style={{
                              fontSize: '1.8rem',
                              fontWeight: 400,
                              lineHeight: '1.5rem',
                            }}
                          >
                            {currencyFormat(39000)}
                          </span>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col
                xs={24}
                md={12}
                style={{
                  padding: '40px',
                }}
              >
                <Row gutter={[0, 24]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Tiền thanh toán:
                    </span>
                  </Col>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '28px',
                        fontWeight: 500,
                        color: 'var(--primary)',
                        lineHeight: '1.5rem',
                      }}
                    >
                      {currencyFormat(cartTotalDiscountPrice + 39000)}
                    </span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>

      <Col span={24}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <ChangeCurrentButton current={2} title="Chọn ngày giao hàng" />
          </Col>
          <Col
            xs={24}
            sm={24}
            style={{
              borderRadius: '4px',
              backgroundColor: 'white',
            }}
          >
            <Row gutter={[0, 16]}>
              <Col
                xs={24}
                md={12}
                style={{
                  padding: '40px',
                  borderRight: '1px solid #ddd',
                }}
              >
                <Row justify="start" gutter={[0, 8]}>
                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 500,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Hình thức thanh toán
                    </span>
                  </Col>

                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 400,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Tiền mặt (Thanh toán khi nhận hàng)
                    </span>
                  </Col>

                  <Col span={24}>
                    <span
                      style={{
                        fontSize: '1.8rem',
                        fontWeight: 400,
                        lineHeight: '1.5rem',
                      }}
                    >
                      Ngày giao hàng: {deliveryDate}
                    </span>
                  </Col>

                  {!!orderNote && (
                    <Col span={24}>
                      <span
                        style={{
                          fontSize: '1.8rem',
                          fontWeight: 400,
                          lineHeight: '1.5rem',
                        }}
                      >
                        Ghi chú khi giao hàng: {orderNote}
                      </span>
                    </Col>
                  )}
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default CheckoutStep4;
