import React, { useState } from 'react';
import { Row, Col, Tabs, Button, Table } from 'antd';

import FooterProductDescription from './components/FooterProductDescription';
import './style.scss';
import 'antd/dist/antd.css';

const ButtonTabs = (props) => {
  const { title } = props;
  return (
    <Button className="button-tabs" type="default">
      {title}
    </Button>
  );
};

const ProductDescription = (props) => {
  const [viewMore, setViewMore] = useState(false);

  const { TabPane } = Tabs;
  const { data } = props;

  const columns = [
    {
      title: 'Tên nguyên liệu',
      dataIndex: 'ingredient',
      key: 'ingredient',
    },
    {
      title: 'Số lượng',
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: 'Đơn vị',
      dataIndex: 'unit',
      key: 'unit',
    },
  ];

  const createArray = (array) => {
    const newArray = [];
    array.forEach((element, index) => {
      newArray.push({
        key: index,
        ...element,
      });
    });
    return newArray;
  };

  return (
    <Row style={{ marginTop: '4rem' }} gutter={[0, 32]} id="description">
      <Col xs={{ span: 24 }}>
        <span
          style={{
            fontSize: '3.2rem',
            fontWeight: '600',
          }}
        >
          Thông tin sản phẩm
        </span>
      </Col>
      <Col xs={{ span: 24 }}>
        <Tabs defaultActiveKey="description" className="tabs-description">
          <TabPane
            key="description"
            tab={<ButtonTabs title="Mô tả sản phẩm" />}
            className={viewMore ? '' : 'tab-description'}
            style={{
              position: 'relative',
              height: viewMore ? 'auto' : '25rem',
              backgroundColor: 'var(--quaternary)',
            }}
          >
            <Row style={{ padding: '2.4rem' }} gutter={[0, 32]}>
              <Col xs={{ span: 24 }}>
                <div
                  style={{ fontSize: '2rem' }}
                  dangerouslySetInnerHTML={{
                    __html: data?.description,
                  }}
                />
              </Col>
              {data?.images?.map((item, index) => (
                <Col key={index} style={{ minWidth: '100%' }}>
                  <img
                    src={process.env.REACT_APP_BACKEND_URL + item?.content}
                    width="100%"
                    alt=""
                  />
                </Col>
              ))}
              <Col xs={{ span: 24 }} style={{ marginBottom: '2rem' }}>
                <FooterProductDescription />
              </Col>
            </Row>
            <Col
              xs={{ span: 24 }}
              style={{
                position: 'absolute',
                bottom: '0',
                zIndex: '10',
                display: 'flex',
                justifyContent: 'center',
                width: '100%',
              }}
            >
              <Row justify="center" align="middle" className="view-more">
                <Button
                  type="primary"
                  className="view-more-btn"
                  size="large"
                  onClick={() => setViewMore(!viewMore)}
                >
                  {viewMore ? 'Thu gọn nội dung' : 'Xem thêm nội dung'}
                </Button>
              </Row>
            </Col>
          </TabPane>
          <TabPane
            key="ingredient"
            tab={<ButtonTabs title="Nguyên liệu" />}
            style={{
              backgroundColor: 'var(--quaternary)',
            }}
          >
            <Row justify="center" align="middle" style={{ padding: '2rem 0' }}>
              {data?.sizes
                ?.sort((a, b) => a?.size?.id - b?.size?.id)
                ?.map((it, idx) => (
                  <Col xs={{ span: 24 }} key={idx}>
                    {!!it?.ingredients?.length ? (
                      <>
                        <span
                          style={{
                            fontSize: '2rem',
                            padding: '0 3.2rem',
                          }}
                        >
                          <strong>{it?.sizeName}</strong>
                        </span>
                        <Table
                          pagination={false}
                          columns={columns}
                          dataSource={createArray(it?.ingredients)}
                          className="ingredient"
                        />
                      </>
                    ) : (
                      <div style={{ fontSize: '24px', padding: '24px 30px' }}>
                        Sản phẩm không có thông tin nguyên liệu, chúng tôi sẽ
                        cập nhật lại sau!
                      </div>
                    )}
                  </Col>
                ))}
            </Row>
          </TabPane>
        </Tabs>
      </Col>
    </Row>
  );
};
export default ProductDescription;
