import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Button, notification } from 'antd';
import { useSelector } from 'react-redux';

import { calculateSalePrice } from 'utils/helper';
import currencyFormat from 'utils/currencyFormat';
import './style.scss';

const ProductPurchase = (props) => {
  const { data } = props;

  const createArray = (array) => {
    const res = [];
    Array.isArray(array) &&
      array.forEach((element) => {
        return res.push({
          quantity: 0,
          dishSize: {
            productId: element?.productId,
            sizeImage: element?.sizeImage,
            sizeName: element?.sizeName,
            sizeId: element?.sizeId,
            price: element?.price,
          },
        });
      });
    return res;
  };
  const initialQuantity = createArray(data?.sizes);

  const { items } = useSelector((store) => store.cart);
  const itemFromCart = items.find((x) => x?.product?.id === data?.id);

  const [arrayQuantityValue, setArrayQuantityValue] = useState(
    itemFromCart?.quantity ?? initialQuantity
  );

  useEffect(() => {
    setArrayQuantityValue(itemFromCart?.quantity ?? initialQuantity);
  }, [data?.slug]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleAddToCart = (e) => {
    e.preventDefault();
    if (
      arrayQuantityValue.reduce((prev, curr) => prev + curr?.quantity, 0) === 0
    ) {
      notification.warning({
        message: 'Vui lòng nhập số lượng món ăn để thêm vào giỏ hàng',
      });
      return;
    }
    return props?.onAddToCart({
      product: {
        id: data?.id,
        name: data?.name,
        images: data?.images,
        discount: data?.discount,
        slug: data?.slug,
      },
      quantity: arrayQuantityValue,
    });
  };

  return (
    <Row className="purchase__container" gutter={[0, 16]}>
      {arrayQuantityValue
        ?.sort((a, b) => a?.dishSize?.sizeId - b?.dishSize?.sizeId)
        ?.map((it, idx) => (
          <Col xs={{ span: 24 }} key={idx}>
            <Row gutter={[0, 12]}>
              <Col xs={{ span: 12 }} className="cart-form__price">
                <span
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    fontSize: '1.5rem',
                    lineHeight: '2.25rem',
                  }}
                >
                  <img
                    src={
                      process.env.REACT_APP_BACKEND_URL +
                      it?.dishSize?.sizeImage
                    }
                    alt="m-size"
                  />
                  &nbsp; {it?.dishSize?.sizeName}
                </span>
              </Col>
              <Col xs={{ span: 12 }} className="cart-form__price">
                <Row
                  justify="space-between"
                  align="center"
                  style={{ width: '100%' }}
                >
                  <Col
                    xs={{ span: 8 }}
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Button
                      className="cart-form-button cart-form-button-down"
                      onClick={() => {
                        arrayQuantityValue[idx].quantity -= 1;
                        setArrayQuantityValue([...arrayQuantityValue]);
                        if (arrayQuantityValue[idx].quantity <= 0) {
                          arrayQuantityValue[idx].quantity = 0;
                          setArrayQuantityValue([...arrayQuantityValue]);
                        }
                      }}
                    >
                      <img src="/assets/images/minus.svg" alt="minus" />
                    </Button>
                  </Col>
                  <Col
                    xs={{ span: 8 }}
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Input
                      size="middle"
                      pattern="[0-9]*"
                      value={arrayQuantityValue[idx].quantity}
                      className="cart-form-quantity"
                    />
                  </Col>
                  <Col
                    xs={{ span: 8 }}
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Button
                      className="cart-form-button cart-form-button-up"
                      onClick={() => {
                        arrayQuantityValue[idx].quantity += 1;
                        setArrayQuantityValue([...arrayQuantityValue]);
                        if (arrayQuantityValue[idx].quantity >= 50) {
                          arrayQuantityValue[idx].quantity = 50;
                          setArrayQuantityValue([...arrayQuantityValue]);
                        }
                      }}
                    >
                      <img src="/assets/images/plus.svg" alt="s-size" />
                    </Button>
                  </Col>
                </Row>
              </Col>
              <Col xs={{ span: 24 }} className="cart-form__price">
                <Row style={{ width: '100%' }}>
                  <Col span={24}>
                    {!!data?.product?.discount ? (
                      <Row>
                        <span style={{ marginRight: '0.8rem' }}>
                          {currencyFormat(
                            calculateSalePrice(
                              data?.product?.discount,
                              it?.quantity * it?.dishSize?.price
                            )
                          )}
                        </span>
                        <span
                          style={{
                            color: 'rgb(120, 120, 120)',
                            textDecoration: 'line-through',
                          }}
                        >
                          {currencyFormat(it?.quantity * it?.dishSize?.price)}
                        </span>
                      </Row>
                    ) : (
                      <Row>
                        <span style={{ marginRight: '0.8rem' }}>
                          {currencyFormat(
                            it?.quantity * it?.dishSize?.price ||
                              it?.dishSize?.price
                          )}
                        </span>
                      </Row>
                    )}
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        ))}

      <Col xs={{ span: 24 }}>
        <Button
          type="primary"
          size="large"
          className="purchase-button"
          onClick={handleAddToCart}
        >
          Đặt món
        </Button>
      </Col>
    </Row>
  );
};
export default ProductPurchase;
