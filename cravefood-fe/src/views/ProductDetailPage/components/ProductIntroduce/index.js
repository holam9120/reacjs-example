import React from 'react';
import { Row, Col, Breadcrumb } from 'antd';

import 'antd/dist/antd.css';
import './style.scss';

const ProductIntroduce = (props) => {
  const { data } = props;

  const handleViewMore = () => {
    let elmnt = document.getElementById('description');
    elmnt.scrollIntoView({
      behavior: 'smooth',
    });
  };

  return (
    <Row style={{ padding: '0.8rem 0' }} gutter={[0, 16]}>
      <Col xs={{ span: 24 }} style={{ marginBottom: '2.4rem' }}>
        <Breadcrumb separator=">">
          <Breadcrumb.Item>Món ngon CraveFood</Breadcrumb.Item>
          <Breadcrumb.Item>
            <span
              style={{ textTransform: 'uppercase', color: 'var(--secondary)' }}
            >
              {data?.name}
            </span>
          </Breadcrumb.Item>
        </Breadcrumb>
      </Col>
      <Col xs={{ span: 24 }}>
        <h2 className="product__name">{data?.name}</h2>
      </Col>
      <Col xs={{ span: 24 }}>
        <span className="product__id">Mã sản phẩm:&nbsp;{data?.id}</span>
      </Col>
      <Col xs={{ span: 24 }}>
        <div
          className="product__des"
          dangerouslySetInnerHTML={{
            __html: `<strong>${data?.name}</strong> - ` + data?.description,
          }}
        />
      </Col>
      <Col xs={{ span: 24 }} style={{ padding: '0' }}>
        <span
          style={{
            fontSize: '1.3rem',
            color: 'var(--primary)',
            cursor: 'pointer',
          }}
          onClick={handleViewMore}
        >
          Xem thêm
        </span>
      </Col>
    </Row>
  );
};
export default ProductIntroduce;
