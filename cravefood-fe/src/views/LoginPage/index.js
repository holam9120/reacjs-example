import React from 'react';

import AccountWrapper from '../../components/AccountWrapper';
import LoginForm from '../../components/AccountWrapper/LoginForm';

const LoginPage = () => {
  const LoginComponent = AccountWrapper(LoginForm);
  return <LoginComponent />;
};
export default LoginPage;
