import React from 'react';

import AccountWrapper from '../../components/AccountWrapper';
import ForgetPasswordForm from '../../components/AccountWrapper/ForgetPasswordForm';

const ForgetPasswordPage = () => {
  const ForgetPasswordComponent = AccountWrapper(ForgetPasswordForm);
  return <ForgetPasswordComponent />;
};
export default ForgetPasswordPage;
