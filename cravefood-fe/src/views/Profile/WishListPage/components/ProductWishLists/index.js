import React, { useEffect } from 'react';
import { Row, Col, Button, Empty } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreator } from 'store/user/user.meta';
import ProductItem from 'components/ProductItem';

const ProductWishList = () => {
  const dispatch = useDispatch();

  const { user = {}, wishLists = [] } = useSelector((store) => store.user);

  const fetchProductsWishList = (id) => {
    dispatch(actionCreator.getProductWishLists({ userId: id }));
  };

  useEffect(() => {
    if (!user) {
      navigate('/');
    } else {
      fetchProductsWishList(user?.id);
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <Row justify="start">
      {wishLists?.length > 0 ? (
        wishLists?.map((it, idx) => (
          <Col xs={12} sm={3} md={6} key={idx}>
            <ProductItem data={it?.product} />
          </Col>
        ))
      ) : (
        <>
          <Col
            xs={24}
            sm={24}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              padding: '30px 0',
            }}
          >
            <Empty description="Chưa có sản phẩm yêu thích nào." />
          </Col>
          <Col xs={24} sm={24}>
            <Row justify="center">
              <Button size="large" type="ghost" onClick={() => navigate('/')}>
                Về trang chủ
              </Button>
            </Row>
          </Col>
        </>
      )}
    </Row>
  );
};

export default ProductWishList;
