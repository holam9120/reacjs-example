import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import ProductWishLists from './components/ProductWishLists';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const WishListPage = () => {
  const WishListComponent = ComponentWrapper(ProductWishLists);
  return (
    <WishListComponent
      data={menuItem?.menuItemAccount}
      title="Danh sách yêu thích"
    />
  );
};
export default WishListPage;
