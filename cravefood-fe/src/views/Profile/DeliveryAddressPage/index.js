import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import AddressList from './components/AddressList';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const DeliveryAddressPage = () => {
  const DeliveryAddressComponent = ComponentWrapper(AddressList);
  return (
    <DeliveryAddressComponent
      data={menuItem?.menuItemAccount}
      title="Địa chỉ giao hàng"
    />
  );
};
export default DeliveryAddressPage;
