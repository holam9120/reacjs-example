import React from 'react';

import menuItem from 'components/ComponentWrapper/menu-item';
import OrderCard from './components/OrderCard';
import ComponentWrapper from 'components/ComponentWrapper';
import 'antd/dist/antd.css';

const OrderPage = () => {
  const OrderComponent = ComponentWrapper(OrderCard);
  return (
    <OrderComponent
      data={menuItem?.menuItemAccount}
      title="Danh sách đơn hàng"
    />
  );
};
export default OrderPage;
