import React from 'react';

import AccountWrapper from '../../components/AccountWrapper';
import RegisterForm from '../../components/AccountWrapper/RegisterForm';

const RegisterPage = () => {
  const RegisterComponent = AccountWrapper(RegisterForm);
  return <RegisterComponent />;
};

export default RegisterPage;
