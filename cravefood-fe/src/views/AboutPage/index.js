import React from 'react';
import { Row, Col } from 'antd';

import './style.scss';

const AboutPage = () => {
  const imageLinks = [
    '/assets/images/about/CraveFood_Hosonangluc-1-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-2-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-3-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-4-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-5-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-6-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-7-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-8-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-9-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-10-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-11-min.jpg',
    '/assets/images/about/CraveFood_Hosonangluc-12-min.jpg',
  ];

  return (
    <Row justify="center">
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 20, offset: 2 }}
        lg={{ span: 20, offset: 2 }}
        style={{ padding: '1.6rem' }}
      >
        <h3 style={{ fontSize: '2.4rem', fontWeight: '600' }}>Về chúng tôi</h3>
      </Col>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 20, offset: 2 }}
        lg={{ span: 20, offset: 2 }}
        style={{ padding: '1.6rem' }}
      >
        <Row justify="center">
          {imageLinks.map((item, index) => (
            <Col xs={{ span: 24 }} key={index}>
              <img
                style={{ maxWidth: '100%' }}
                src={item}
                alt={`img-about-${index}`}
              />
            </Col>
          ))}
        </Row>
      </Col>
    </Row>
  );
};

export default AboutPage;
