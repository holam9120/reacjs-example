import React, { useState } from 'react';
import { Row, Col, Badge, Drawer } from 'antd';
import { useSelector } from 'react-redux';

import CartContainer from '../CartContainer';

import './style.scss';

const FloatingCart = () => {
  const [visible, setVisible] = useState(false);
  const { total } = useSelector((store) => store.cart);

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="floating__cart">
      <Row justify="end">
        <Col style={{ marginRight: '2.4rem' }}>
          <Badge count={total ?? 0} className="cart">
            <Row onClick={showDrawer}>
              <img
                className="cart__image"
                src="/assets/images/cart.png"
                alt="cart"
              />
            </Row>
            <Drawer
              title="Giỏ hàng"
              placement="right"
              width={375}
              closable={true}
              onClose={onClose}
              visible={visible}
            >
              <CartContainer />
            </Drawer>
          </Badge>
        </Col>
      </Row>
    </div>
  );
};
export default FloatingCart;
