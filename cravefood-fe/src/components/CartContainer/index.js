import React from 'react';
import { Row, Col, Avatar, Button } from 'antd';
import { useSelector } from 'react-redux';
import { navigate } from '@reach/router';

import CartItem from '../CartItem';
import currencyFormat from 'utils/currencyFormat';

import './style.scss';

const CartContainer = () => {
  const currentCart = useSelector((store) => store.cart);

  return (
    <Row gutter={[0, 16]}>
      <Col xs={{ span: 24 }}>
        <Avatar size={40} src="/assets/images/avatar-placeholder.jpg" />
      </Col>
      <Col
        xs={{ span: 24 }}
        style={{
          padding: '1rem 1.6rem',
          borderRadius: '0.4rem',
          fontSize: '1.6rem',
          backgroundColor: 'rgb(238, 238, 238)',
        }}
      >
        {currentCart?.total} món lẻ
      </Col>

      {!!currentCart?.items.length &&
        currentCart?.items.map((item) => (
          <Col key={item?.product?.id}>
            <CartItem item={item} />
          </Col>
        ))}

      <Col
        xs={{ span: 24 }}
        style={{
          marginTop: '3rem',
          padding: '1.6rem 1rem',
          borderRadius: '0.4rem',
          fontSize: '1.6rem',
          backgroundColor: 'rgb(255, 228, 204)',
        }}
      >
        <Row justify="space-between" align="middle">
          <Col xs={{ span: 12 }}>
            <Row>Tổng giá</Row>
          </Col>
          <Col xs={{ span: 12 }}>
            <Row justify="end">
              {currencyFormat(currentCart?.cartTotalDiscountPrice)}
            </Row>
          </Col>
        </Row>
      </Col>

      {!!currentCart?.items.length && (
        <Col xs={{ span: 24 }} style={{ marginTop: '2.4rem' }}>
          <Button
            type="primary"
            size="large"
            className="cart-item-button"
            style={{ width: '100%' }}
            onClick={() => navigate('/thanh-toan')}
          >
            Tiến hành mua hàng
          </Button>
        </Col>
      )}
    </Row>
  );
};

export default CartContainer;
