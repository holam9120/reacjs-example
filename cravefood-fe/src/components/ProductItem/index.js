import React, { useState } from 'react';
import { Row, Col, notification } from 'antd';
import { Link, navigate } from '@reach/router';
import { useSelector, useDispatch } from 'react-redux';

import { actionCreator } from 'store/user/user.meta';
import CartEditModal from '../CartEditModal';
import { calculateSalePrice } from 'utils/helper';
import currencyFormat from 'utils/currencyFormat';

import './style.scss';

const ProductItem = (data) => {
  require('dotenv').config();
  const dispatch = useDispatch();

  const productData = data.data ?? data;

  const createArray = (array) => {
    const res = [];
    array.forEach((element) => {
      return res.push({
        quantity: 0,
        dishSize: {
          productId: element?.productId,
          sizeImage: element?.sizeImage,
          sizeName: element?.sizeName,
          sizeId: element?.sizeId,
          price: element?.price,
        },
      });
    });
    return res;
  };

  const initialQuantity = createArray(productData?.sizes);

  const initialItem = {
    product: {
      id: productData?.id,
      name: productData?.name,
      images: productData?.images,
      discount: productData?.discount,
      slug: productData?.slug,
    },
    quantity: initialQuantity,
  };

  const [active, setActive] = useState(false);
  const [visible, setVisible] = useState(false);

  const { user = {} } = useSelector((store) => store.user);

  const itemFromStoreByProductId = useSelector((store) =>
    store.cart.items.find((x) => x.product.id === productData?.id)
  );

  const handleCloseModal = (vi) => {
    setVisible(vi);
  };

  return (
    <div
      style={{
        padding: '0 0.4rem',
        marginTop: '1.2rem',
        marginBottom: '2.4rem',
      }}
    >
      <div
        className="product-card__container"
        onMouseMove={() => setActive(true)}
        onMouseLeave={() => setActive(false)}
      >
        <Row
          className="product-card__image"
          style={{
            backgroundImage: `url('${
              process.env.REACT_APP_BACKEND_URL +
              productData?.images?.[0]?.content
            }')`,
          }}
          onClick={() => navigate(`/mon-le/chi-tiet/${productData?.slug}`)}
        ></Row>
        {user?.wishLists?.includes(productData?.id) ? (
          <img
            src="/assets/images/like.png"
            alt="hit-tags"
            className="product-card__image--unlike"
            onClick={() => {
              if (user?.id) {
                dispatch(
                  actionCreator.removeWishLists({
                    userId: user?.id,
                    productId: productData?.id,
                  })
                );
              }
            }}
          />
        ) : (
          <img
            src="/assets/images/unlike.png"
            alt="hit-tags"
            className="product-card__image--unlike"
            onClick={() => {
              if (user?.id) {
                dispatch(
                  actionCreator.addWishLists({
                    userId: user?.id,
                    productId: productData?.id,
                  })
                );
              } else {
                notification.warning({
                  message:
                    'Vui lòng đăng nhập để thêm sản phẩm vào danh sách yêu thích',
                });
                return;
              }
            }}
          />
        )}
        {!active ? (
          <div></div>
        ) : (
          <div
            className="product-card__image--styled"
            onClick={() => {
              setVisible(true);
            }}
          >
            <span className="product-card__discount--content">
              {itemFromStoreByProductId?.quantity?.reduce(
                (prev, curr) => prev + curr?.quantity,
                0
              ) ?? 0}
            </span>
            <Row justify="center">
              <img
                src="/assets/images/cart.png"
                alt="add-cart"
                className="add-cart"
              />
            </Row>
          </div>
        )}
        <Row>
          <CartEditModal
            item={itemFromStoreByProductId ?? initialItem}
            visible={visible}
            onClose={handleCloseModal}
          />
        </Row>
        <div
          className="product-card__description"
          onClick={() => navigate(`/mon-le/chi-tiet/${productData?.slug}`)}
        >
          <Link to="/" className="product-card__description--name">
            {productData?.name}
          </Link>
          <Row align="top">
            <Col sx={{ span: 24 }}>
              {productData?.sizes
                ?.sort((a, b) => a?.sizeId - b?.sizeId)
                ?.map((it) => (
                  <Row
                    align="middle"
                    style={{ marginTop: '0.6rem' }}
                    key={it?.sizeId}
                  >
                    <img
                      src={process.env.REACT_APP_BACKEND_URL + it?.sizeImage}
                      alt="s-size"
                    />
                    <span className="product-card__description--price">
                      {currencyFormat(
                        calculateSalePrice(productData?.discount, it?.price)
                      )}
                    </span>
                  </Row>
                ))}
            </Col>
          </Row>
        </div>
        {(productData?.discount || productData?.discount !== 0) && (
          <div className="product-card__discount">
            <span className="product-card__discount--content">
              -{productData?.discount * 100}%
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductItem;
