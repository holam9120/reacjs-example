import React, { useState } from 'react';
import { Row, Col, Divider, Modal } from 'antd';
import { useDispatch } from 'react-redux';
import { Link } from '@reach/router';

import CartEditModal from '../CartEditModal';
import { actionCreators } from '../../store/cart/cart.meta';
import { calculateSalePrice } from 'utils/helper';
import currencyFormat from 'utils/currencyFormat';

import './style.scss';

const CartItem = (props) => {
  const { item, showButton = true } = props;
  const dispatch = useDispatch();

  const [visible, setVisible] = useState(false);
  const { confirm } = Modal;

  const handleCloseModal = (vi) => {
    setVisible(vi);
  };

  return (
    <Col xs={{ span: 24 }}>
      <Row justify="space-between" gutter={[0, 16]}>
        <Col xs={{ span: 7 }}>
          <img
            src={
              process.env.REACT_APP_BACKEND_URL +
              item?.product?.images?.[0]?.content
            }
            alt="product-img"
            style={{ maxWidth: '100%', minHeight: '50%' }}
          />
        </Col>
        <Col xs={{ span: 17 }}>
          <Row align="top" gutter={[0, 8]} style={{ padding: '0 0.8rem' }}>
            <Col xs={{ span: 24 }}>
              <Link to={`/mon-le/chi-tiet/${item?.product?.slug}`}>
                <span
                  style={{
                    fontSize: '1.8rem',
                    textTransform: 'uppercase',
                    cursor: 'pointer',
                  }}
                >
                  {item?.product?.name}
                </span>
              </Link>
            </Col>

            {item?.quantity
              ?.sort((a, b) => a?.dishSize?.sizeId - b?.dishSize?.sizeId)
              ?.map(
                (it, idx) =>
                  !!it?.quantity && (
                    <Col xs={{ span: 24 }} key={idx}>
                      <Row justify="space-between">
                        <Col>
                          <img
                            src={
                              process.env.REACT_APP_BACKEND_URL +
                              it?.dishSize?.sizeImage
                            }
                            alt="s-size"
                          />
                          <span>
                            &nbsp;
                            <strong>x{it?.quantity}</strong>
                          </span>
                        </Col>
                        <Col
                          style={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flex: '1 1 0%',
                            marginLeft: '0.8rem',
                          }}
                        >
                          {!!item?.product?.discount ? (
                            <Row>
                              <span style={{ marginRight: '0.8rem' }}>
                                {currencyFormat(
                                  calculateSalePrice(
                                    item?.product?.discount,
                                    it?.quantity * it?.dishSize?.price
                                  )
                                )}
                              </span>
                              <span
                                style={{
                                  color: 'rgb(120, 120, 120)',
                                  textDecoration: 'line-through',
                                }}
                              >
                                {currencyFormat(
                                  it?.quantity * it?.dishSize?.price
                                )}
                              </span>
                            </Row>
                          ) : (
                            <Row>
                              <span style={{ marginRight: '0.8rem' }}>
                                {currencyFormat(
                                  it?.quantity * it?.dishSize?.price
                                )}
                              </span>
                            </Row>
                          )}
                        </Col>
                      </Row>
                    </Col>
                  )
              )}

            {showButton && (
              <Col xs={{ span: 24 }}>
                <Row justify="end" style={{ width: '100%' }}>
                  <Col
                    className="cart-btn cart-btn__edit"
                    onClick={() => {
                      setVisible(true);
                    }}
                  >
                    <img src="/assets/images/pencil.png" alt="pencil" />
                  </Col>
                  <CartEditModal
                    item={item}
                    visible={visible}
                    onClose={handleCloseModal}
                  />
                  <Col
                    className="cart-btn cart-btn__remove"
                    onClick={() => {
                      confirm({
                        okText: 'Đồng ý',
                        cancelText: 'Hủy',
                        title: 'Bạn có muốn xóa món ăn ra khỏi giỏ hàng?',
                        onOk() {
                          dispatch(
                            actionCreators.actRemoveProductFromCart(
                              item?.product?.id
                            )
                          );
                        },
                      });
                    }}
                  >
                    <img src="/assets/images/trash-bin.png" alt="trash-bin" />
                  </Col>
                </Row>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
      <Row style={{ width: '100%' }}>
        <Divider className="cart-item-divider"></Divider>
      </Row>
    </Col>
  );
};

export default CartItem;
