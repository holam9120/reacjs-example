import React, { useEffect, useState } from 'react';
import { Row, Col, Dropdown, Menu, Modal } from 'antd';
import { DownOutlined, SearchOutlined, UserOutlined } from '@ant-design/icons';
import { Link } from '@reach/router';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreator as userActions } from 'store/user/user.meta';
import { actionCreators } from 'store/globalValue/global.meta';
import './style.scss';

const StyledSpan = styled.span`
  &:hover {
    color: var(--secondary);
  }
`;

const DropdownMenu = (props) => {
  const { name, data, option = 'mon-le/' } = props;

  const menu = (
    <Menu className="sub__menu">
      {data.map((item) => (
        <Menu.Item className="sub__menu-item" key={item?.id}>
          {item?.onClick ? (
            <StyledSpan onClick={item?.onClick}>{item?.name}</StyledSpan>
          ) : (
            <Link to={`${option}${item?.slug}`}>{item?.name}</Link>
          )}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <Link to="/" className="menu__item">
        {name}&nbsp;
        <DownOutlined style={{ fontSize: '1rem' }} />
      </Link>
    </Dropdown>
  );
};

const HeaderClient = () => {
  const dispatch = useDispatch();
  const { confirm } = Modal;

  const [isUserLogin, setIsUserLogin] = useState(false);

  const { user = {} } = useSelector((store) => store.user);

  const { categories = [] } = useSelector(
    (store) => store.globalValue.globalValue
  );

  const fetchCategories = () => {
    dispatch(actionCreators.actFetchGlobalValue());
  };

  useEffect(() => {
    if (!!user) {
      setIsUserLogin(true);
    }
  }, [user]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    fetchCategories();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const subMenu = [
    [{ id: 'mon-le', name: 'Món lẻ', slug: 'mon-le' }, ...categories],
    [
      {
        id: 'giao-mon-dung-cu-phuc-vu-thu-don-ve-sinh',
        name: 'Giao món, dụng cụ, phục vụ, thu dọn, vệ sinh',
        slug: '/',
      },
      {
        id: 'tiec-luu-dong',
        name: 'Tiệc trọn gói Lưu động bất kỳ',
        slug: '/',
      },
    ],
    [
      { id: 'ban-tin', name: 'Bản tin', slug: '/' },
      { id: 'cau-hoi-pho-bien', name: 'Câu hỏi phổ biến', slug: '/' },
    ],
    [
      {
        id: 'tai-khoan',
        name: 'Tài khoản',
        slug: '/thong-tin-ca-nhan',
      },
      {
        id: 'don-hang',
        name: 'Đơn hàng',
        slug: '/danh-sach-don-hang',
      },
      {
        id: 'delivery-addresses',
        name: 'Địa chỉ giao hàng',
        slug: '/dia-chi-giao-hang',
      },
      {
        id: 'wish-list',
        name: 'Yêu thích',
        slug: '/danh-sach-yeu-thich',
      },
      {
        id: 'logout',
        name: 'Đăng xuất',
        onClick: () => {
          confirm({
            okText: 'Đăng xuất',
            cancelText: 'Hủy thao tác',
            title:
              'Bạn có chắc bạn muốn đăng xuất khỏi tài khoản CRAVEFOOD không? Có thể bạn sẽ mất đi một vài ưu đãi đặc biệt dành riêng cho tài khoản của CRAVEFOOD nhé!',
            onOk() {
              dispatch(userActions.logout());
              setIsUserLogin(false);
            },
          });
        },
      },
    ],
  ];

  return (
    <Row justify="space-between" style={{ width: '100%', flexWrap: 'nowrap' }}>
      <Col className="logo">
        <Link to="/" className="logo-link">
          <span className="logo-title" style={{ margin: 0 }}>
            CraveFood
          </span>
        </Link>
      </Col>
      <Col className="menu">
        <Link to="/" className="menu__item active">
          Trang chủ
        </Link>
        <DropdownMenu name="Món ngon CraveFood" data={subMenu?.[0]} />
        <DropdownMenu name="Dịch vụ tiệc" data={subMenu?.[1]} />
        <DropdownMenu name="Tin tức" data={subMenu?.[2]} />
        <Link to="/lien-he" className="menu__item">
          Liên hệ
        </Link>
      </Col>
      <Col className="right-group">
        <Row className="button-search">
          <Link to="/tim-kiem">
            <SearchOutlined />
          </Link>
        </Row>
        <Row className="button-login">
          <Link to="/dang-nhap">
            <UserOutlined />
          </Link>
        </Row>
        {!isUserLogin ? (
          <></>
        ) : (
          <Row>
            <DropdownMenu name="Chào bạn" data={subMenu?.[3]} option="" />
          </Row>
        )}
      </Col>
    </Row>
  );
};
export default HeaderClient;
