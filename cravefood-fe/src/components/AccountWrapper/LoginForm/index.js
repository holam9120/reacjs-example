import React, { useEffect } from 'react';
import { Row, Col, Card, Form, Input, Button, Divider } from 'antd';
import { navigate } from '@reach/router';
import { useDispatch, useSelector } from 'react-redux';

import { actionCreator } from 'store/user/user.meta';
import 'antd/dist/antd.css';
import '../style.scss';

const LoginForm = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { user = {} } = useSelector((store) => store.user);

  useEffect(() => {
    if (!!user) {
      navigate('/');
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  const onFinish = (values) => {
    dispatch(
      actionCreator.login({
        ...values,
      })
    );
  };

  return (
    <Card bordered>
      <Form form={form} scrollToFirstError onFinish={onFinish}>
        <Row className="account-form__container">
          <Col span={24} className="account-form__title">
            <h2>Đăng nhập</h2>
          </Col>
          <Col
            span={24}
            className="account-form__item"
            style={{ marginTop: '2.4rem' }}
          >
            <span>Email</span>
            <Form.Item
              name="email"
              rules={[
                {
                  type: 'email',
                  message: 'Email không hợp lệ',
                },
                {
                  required: true,
                  message: 'Vui lòng điền email',
                },
              ]}
              className="account-input"
            >
              <Input size="large" placeholder="Điền email" />
            </Form.Item>
          </Col>

          <Col span={24} className="account-form__item">
            <span>Mật khẩu</span>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Hãy nhập password',
                },
              ]}
              hasFeedback
              className="account-input"
            >
              <Input.Password size="large" placeholder="Điền password" />
            </Form.Item>
          </Col>

          <Col span={24}>
            <span style={{ fontSize: '1.4rem', fontWeight: '500' }}>
              Quên mật khẩu?&nbsp;
              <span
                style={{ color: 'var(--secondary)', cursor: 'pointer' }}
                onClick={() => navigate('/quen-mat-khau')}
              >
                Cài đặt lại mật khẩu
              </span>
            </span>
          </Col>

          <Col span={24} style={{ marginTop: '2.4rem' }}>
            <Button
              type="primary"
              size="large"
              className="button-form"
              style={{ width: '100%' }}
              htmlType="submit"
            >
              Đăng nhập
            </Button>
          </Col>
          <Divider></Divider>
          <Col span={24}>
            <span style={{ fontSize: '1.6rem', fontWeight: '500' }}>
              Chưa có tài khoản?&nbsp;
              <span
                onClick={() => navigate('/dang-ky')}
                style={{ color: 'var(--secondary)', cursor: 'pointer' }}
              >
                Đăng ký
              </span>
            </span>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default LoginForm;
