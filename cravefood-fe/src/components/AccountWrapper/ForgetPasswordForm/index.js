import React from 'react';
import { Row, Col, Card, Form, Input, Button } from 'antd';

import HeadPhoneNumber from '../HeadPhoneNumber';
import 'antd/dist/antd.css';
import '../style.scss';

const ForgetPasswordForm = () => {
  const [form] = Form.useForm();

  return (
    <Card bordered>
      <Form form={form} scrollToFirstError>
        <Row className="account-form__container">
          <Col span={24} className="account-form__title">
            <h2>Quên mật khẩu</h2>
          </Col>

          <Col
            span={24}
            className="account-form__item"
            style={{ marginTop: '2.4rem' }}
          >
            <span style={{ fontSize: '1.4rem', fontWeight: '1.25rem' }}>
              {`Chỉ cần nhập số điện thoại của bạn trong các mẫu dưới đây và bấm
              vào nút "Gửi OTP" và chúng tôi sẽ gửi OTP đến số điện thoại của
              bạn ngay lập tức.`}
            </span>
          </Col>

          <Col
            span={24}
            className="account-form__item"
            style={{ marginTop: '2.4rem' }}
          >
            <span>Số điện thoại</span>
            <Form.Item
              name="phone"
              rules={[
                {
                  required: true,
                  message: 'Hãy nhập số điện thoại',
                },
              ]}
            >
              <Input
                size="large"
                addonBefore={<HeadPhoneNumber />}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>

          <Col span={24} style={{ marginTop: '2.4rem' }}>
            <Button
              type="primary"
              size="large"
              className="button-form"
              style={{ width: '100%' }}
            >
              Gửi OTP
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
export default ForgetPasswordForm;
