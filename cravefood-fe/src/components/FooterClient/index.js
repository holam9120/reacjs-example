import React from 'react';
import { Row, Col, Input } from 'antd';
import { Link } from '@reach/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebook,
  faYoutube,
  faGithub,
  faGitlab,
} from '@fortawesome/free-brands-svg-icons';

import 'antd/dist/antd.css';
import './style.scss';

const FooterClient = () => {
  const { Search } = Input;

  const siteList = [
    {
      title: 'Thông tin',
      linkList: [
        {
          title: 'Về chúng tôi',
          linkTo: '/ve-chung-toi',
        },
        {
          title: 'Chính sách bảo mật',
          linkTo: '/ho-tro-khach-hang/chinh-sach-bao-mat',
        },
        {
          title: 'Điều khoản sử dụng',
          linkTo: '/ho-tro-khach-hang/dieu-khoan-su-dung',
        },
        {
          title: 'Chính sách đổi trả',
          linkTo: '/ho-tro-khach-hang/chinh-sach-doi-tra',
        },
        {
          title: 'Hướng dẫn đặt hàng',
          linkTo: '/ho-tro-khach-hang/huong-dan-dat-hang',
        },
      ],
    },
    {
      title: 'Dịch vụ nổi bật',
      linkList: [
        {
          title: 'Món lẻ',
          linkTo: '/mon-le',
        },
        {
          title: 'Giao món, dụng cụ, phục vụ, thu dọn, vệ sinh',
          linkTo: '/',
        },
        {
          title: 'Tiệc trọn gói Lưu động bất kỳ đâu',
          linkTo: '/',
        },
      ],
    },
    {
      title: 'Hỗ trợ khách hàng',
      linkList: [
        {
          title: 'Thông tin chuyển khoản',
          linkTo: '/ho-tro-khach-hang/thong-tin-chuyen-khoan',
        },
        {
          title: 'Phí giao hàng',
          linkTo: '/ho-tro-khach-hang/thong-tin-giao-hang',
        },
        {
          title: 'Câu hỏi thường gặp',
          linkTo: '/',
        },
        {
          title: 'Bảng tin công ty',
          linkTo: '/',
        },
        {
          title: 'Video',
          linkTo: '/',
        },
      ],
    },
  ];

  return (
    <Row className="footer__container">
      <Col xs={{ span: 24 }}>
        <Row className="footer__contact" justify="space-between" align="middle">
          <Col xs={{ span: 20, offset: 2 }}>
            <Row>
              <Col
                xs={{ span: 24 }}
                md={{ span: 17 }}
                className="contact__left"
              >
                <Row>
                  <span className="contact__left--content">
                    Đăng ký nhận tin từ{' '}
                    <span style={{ color: 'var(--secondary' }}>CraveFood</span>
                  </span>
                </Row>
                <Row>
                  <span className="contact__left--content">
                    Hưởng quyền lợi giảm giá riêng biệt
                  </span>
                </Row>
              </Col>
              <Col
                xs={{ span: 24 }}
                md={{ span: 7 }}
                className="contact__right"
              >
                <Search
                  placeholder="Nhập email của bạn"
                  enterButton="Gửi"
                  size="large"
                  onSearch={(value) => console.log(value)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row
          className="footer__information"
          justify="space-between"
          align="middle"
        >
          <Col xs={{ span: 20, offset: 2 }}>
            <Row>
              <Col
                xs={{ span: 24 }}
                md={{ span: 17 }}
                className="information__left"
              >
                <Row gutter={[4, 0]}>
                  {siteList.map((item, index) => (
                    <Col xs={{ span: 8 }} key={index}>
                      <div style={{ marginBottom: '1rem' }}>
                        <span
                          style={{
                            fontSize: '1.8rem',
                            fontWeight: '600',
                          }}
                        >
                          {item?.title}
                        </span>
                      </div>
                      <ul>
                        {item?.linkList.map((item, index) => (
                          <li style={{ margin: '0.3rem 0' }} key={index}>
                            <Link to={item?.linkTo}>
                              <span style={{ fontSize: '1.5rem' }}>
                                {item?.title}
                              </span>
                            </Link>
                          </li>
                        ))}
                      </ul>
                    </Col>
                  ))}
                </Row>
              </Col>
              <Col
                xs={{ span: 24 }}
                md={{ span: 7 }}
                className="contact__right"
              >
                <Row justify="center" align="middle">
                  <div
                    className="fb-page"
                    data-href="https://www.facebook.com/Crave-Food-101326401742632"
                    data-tabs="D&#xf2;ng th&#x1edd;i gian, th&#xed;ch"
                    data-width="340px"
                    data-height=""
                    data-small-header="false"
                    data-adapt-container-width="false"
                    data-hide-cover="false"
                    data-show-facepile="true"
                  >
                    <blockquote
                      cite="https://www.facebook.com/Crave-Food-101326401742632"
                      className="fb-xfbml-parse-ignore"
                    >
                      <a href="https://www.facebook.com/Crave-Food-101326401742632">
                        Facebook
                      </a>
                    </blockquote>
                  </div>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row className="footer__about">
          <Col
            xs={{ span: 22, offset: 1 }}
            md={{ span: 14, offset: 1 }}
            className="footer__about--content"
          >
            <span style={{ fontSize: '2.4rem', color: 'var(--secondary)' }}>
              CraveFood - Phát huy truyền thống, kết nối hiện đại
            </span>
            <span className="content-infor">Công ty cổ phần CraveFood</span>
            <span className="content-infor">
              Hẻm 58/15 đường số 5, khu phố 2, phường Linh Trung, quận Thủ Đức
            </span>
            <span className="content-infor">
              nhathuynh25798@gmail.com -&nbsp;
              <span style={{ color: 'var(--primary)' }}>
                <a href="https://cravefood.netlify.app/">
                  cravefood.netlify.app
                </a>
              </span>
            </span>
            <span className="content-infor">
              Số điện thoại liên hệ: 0343 661 688
            </span>
            <span className="content-infor">
              Copyright © 2020&nbsp;
              <span style={{ color: 'var(--secondary)' }}>CraveFood</span>
            </span>
            <span className="content-infor">© 2020 Make by Trong Nhat</span>
          </Col>
          <Col
            xs={{ span: 22, offset: 1 }}
            md={{ span: 6 }}
            className="footer__about--certificate"
          >
            <Row justify="end">
              {/* <Col
                xs={{ span: 24 }}
                style={{ display: 'flex', justifyContent: 'end' }}
              >
                <Link to='/'>
                  <img
                    style={{ width: '25rem' }}
                    src='/assets/images/logoSaleNoti.png'
                    alt='logo-sale-noti'
                  />
                </Link>
              </Col> */}
              <Col
                xs={{ span: 24 }}
                style={{
                  display: 'flex',
                  justifyContent: 'end',
                  margin: '2rem',
                }}
              >
                <a href="https://github.com/NhatHuynh25798/">
                  <FontAwesomeIcon
                    icon={faGithub}
                    style={{
                      margin: '0 1rem',
                      fontSize: '4.8rem',
                      color: 'var(--tertiary',
                    }}
                  />
                </a>
                <a href="https://gitlab.com/nhathuynh25798/">
                  <FontAwesomeIcon
                    icon={faGitlab}
                    style={{
                      margin: '0 1rem',
                      fontSize: '4.8rem',
                      color: '#FC6D27',
                    }}
                  />
                </a>

                <a href="https://www.facebook.com/huynh.trongnhat/">
                  <FontAwesomeIcon
                    icon={faFacebook}
                    style={{
                      margin: '0 1rem',
                      fontSize: '4.8rem',
                      color: '#3b5898',
                    }}
                  />
                </a>
                <a href="https://www.youtube.com/">
                  <FontAwesomeIcon
                    icon={faYoutube}
                    style={{
                      margin: '0 1rem',
                      fontSize: '4.8rem',
                      color: 'red',
                    }}
                  />
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default FooterClient;
