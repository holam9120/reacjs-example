const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index.tsx", // Dẫn tới file index.js ta đã tạo
  // nếu muốn có nhiều entry thì truyền vào mảng
  output: {
    path: path.join(__dirname, "/build"), // Thư mục chứa file được build ra
    filename: "bundle.js", // Tên file được build ra,
    clean: true  // xóa những file thừa k dùng đến
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/, // Sẽ sử dụng babel-loader cho những file .js( phạm vi file sẽ áp dụng)
        exclude: /node_modules/, // Loại trừ thư mục node_modules
        use: ["babel-loader"], // loader nào sẽ dùng, webpack chỉ đọc được file js và json, them vào để  tiên xử lý
      },
      {
        test: /\.(css|scss)$/, // Sử dụng style-loader, css-loader cho file .css
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  // Chứa các plugins sẽ cài đặt trong tương lai
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html"
    }),
  ], // tạo ra html, js tự động, phát hiện sự thay đỏi của file 
  resolve: {
    extensions: [".tsx", ".ts", ".js", "jsx"],
  },
  devtool: 'inline-source-map', // thông báo cụ thể lỗi do file nào ( nếu set mode : 'development' thì cũng tương tư)
  
  // trỏ đến thư mục build để k cần có hậu tố  index.html khi chạy code
  // devServer: {
  //   compress: isProduction,
  //   port: 9000, // đổi cổng 
  //   hot: true,
  //   open: true,
  //   historyApiFallback: true,
  //   contentBase: './dist' 
  // },

  // xử lý biệc khi sử dụng thư viện trong nhiều file thì chỉ tạo ra 1 file cho thư viện
  // optimization:{  
  //   splitChunks:{
  //     chunks:'all'
  //   }
  // }
  // mode: chọn môi trường làm việc
  // watch: có theo dõi liên tục không( thường config ở package.json)
};

// babel-core: Chuyển đổi ES6 về ES5
// babel-loader: Cho phép chuyển các files Javascript sử dụng Babel & Webpack
// babel-preset-env: Cài đặt sẵn giúp bạn sử dụng Javascript mới nhất trên nhiều môi trường khác nhau (nhiều trình duyệt khác nhau). support truyển đổi ES6, ES7, ES8, ES… về ES5.
// babel-preset-react: Hỗ trợ chuyển đổi JSX về Javascript
// css-loader style-loader: giúp webpack có thể tải file .css dưới dạng module.
// File .babelrc: cấu hình cho thư viện Babel.
// html-webpack-plugin: hỗ trợ tự động tạo mới file bundle khi build dự vào phần output
// webpack-dev-server: hoạt động giống live serve


// config khi start
// +  --open: auto open khi chạy code
// + --hot: sửa lại cập nhật kết quả luôn
// + --mode development : chọn môi trường chạy
// + --watch: luôn lắng nghe sự thay đổi => bundle luôn



// https://dev.to/shivampawar/setup-react-application-using-typescript-and-webpack-2kn6
// https://www.youtube.com/watch?v=145Po5_r0e4&list=PLWMM2vKkNM5gpiq7YKFOqqBIkvXzfIHu6&index=1
// https://fullstack.edu.vn/blog/phan-1-tao-du-an-reactjs-voi-webpack-va-babel.html