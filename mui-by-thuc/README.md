# SOA React Admin

- A CMS UI project to manager SOA token

## Setup Environment

- Setup [API Gateway](https://gitlab.com/aibles-group/bsoa/backend/api-gateway-admin) in the local
- cp .env.local .env
- `yarn` or `npm install`

## Implement

- `yarn start` or `npm run start`

## Login

- email: ngochuynh1991@gmail.com
- password: abcd1234

## Technology

- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Typescript](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
- [React Router Dom](https://reactrouter.com/web)
- [Redux Hook](https://react-redux.js.org/api/hooks)
- [Ant Design](https://ant.design/)
