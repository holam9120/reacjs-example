import React from "react"
import ReactDOM from "react-dom"
import { ThemeProvider } from "@material-ui/styles"
import { ToastContainer } from "react-toastify"
import App from "./App/App"
import * as serviceWorker from "./serviceWorker"
import Themes from "./themes"
import UserProvider from "./context/UserContext"
import "react-toastify/dist/ReactToastify.css"

ReactDOM.render(
  <React.StrictMode>
    <UserProvider>
      <ThemeProvider theme={Themes.default}>
        <App />
        <ToastContainer />
      </ThemeProvider>
    </UserProvider>
  </React.StrictMode>,
  document.getElementById("root")
)

serviceWorker.unregister()
