import React from "react"
import LoginComponent from "../../components/Login"
import AuthLayout from "../../layouts/AuthLayout"

const Login = () => {
  return (
    <AuthLayout>
      <LoginComponent />
    </AuthLayout>
  )
}
export default Login
