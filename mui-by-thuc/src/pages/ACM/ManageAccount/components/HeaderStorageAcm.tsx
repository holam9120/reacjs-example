import { useState } from "react"
import { Button } from "@mui/material"
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline"
import useAxios from "axios-hooks"
import HeaderContent from "src/components/HeaderContent"
import PopupConfirm from "src/components/PopupConfirm"
import { notification } from "src/utils/notification"
import {
  MESSAGE_DELETE_ACCOUNT,
  MESSAGE_DELETE_ACCOUNT_FAIL
} from "src/constants/index"
import { useManageAccount } from "../index"
import { useStyles } from "../styles"

export default function HeaderAction() {
  const [openPopup, setOpenPopup] = useState<boolean>(false)
  const classes = useStyles()
  const { accounts, accountDeletes, setAccounts, setAccountDeletes }: any =
    useManageAccount()

  const [{ data: resListAccount, loading, error }, refetchDeleteAccount] =
    useAxios(
      {
        url: `/storeAccount/deleteAccount?id=8`,
        method: "DELETE"
      },
      { manual: true }
    )

  const handleDelete = () => {
    setOpenPopup(!openPopup)
    const finalData = accounts?.filter(
      item => !accountDeletes.includes(item?.account_id)
    )

    refetchDeleteAccount()
      .then(() => {
        setAccounts(finalData)
        setAccountDeletes([])
        notification({
          message: MESSAGE_DELETE_ACCOUNT,
          type: "success"
        })
      })
      .catch(() =>
        notification({
          message: MESSAGE_DELETE_ACCOUNT_FAIL,
          type: "error"
        })
      )
  }

  return (
    <>
      <PopupConfirm
        open={openPopup}
        onSubmit={handleDelete}
        type="warning"
        onClose={() => setOpenPopup(!openPopup)}
        title="Thông báo"
        text1="Xóa tài khoản"
        text2="Tài khoản này sẽ bị xóa vĩnh viễn. 
        Bạn có chắc chắn xóa tài khoản?"
      />
      <HeaderContent
        actions={[
          <Button
            className={classes.buttonAction}
            variant="contained"
            startIcon={<DeleteOutlineIcon />}
            onClick={() => setOpenPopup(!openPopup)}
            disabled={!!!accountDeletes.length}
          >
            Xóa
          </Button>
        ]}
      />
    </>
  )
}
