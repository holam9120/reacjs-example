import { useState, useEffect } from "react"
import {
  Box,
  Button,
  Divider,
  Menu,
  MenuItem,
  Paper,
  Checkbox
} from "@mui/material"
import { useNavigate, useSearchParams } from "react-router-dom"
import AddIcon from "@mui/icons-material/Add"
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline"
import FilterAltIcon from "@mui/icons-material/FilterAlt"
import useAxios from "axios-hooks"
import { PATH } from "src/constants/paths"
import HeaderContent from "src/components/HeaderContent"
import PopupConfirm from "src/components/PopupConfirm"
import { notification } from "src/utils/notification"
import {
  MESSAGE_DELETE_ACCOUNT,
  MESSAGE_DELETE_ACCOUNT_FAIL
} from "src/constants/index"
import CreateAccount from "./CreateAccount"
import { useManageAccount } from "../index"
import { useStyles } from "../styles"

export default function HeaderListAccount() {
  const [checkedBank, setCheckedBank] = useState<boolean>(false)
  const [checkedCash, setCheckedCash] = useState<boolean>(false)
  const [openPopup, setOpenPopup] = useState<boolean>(false)
  const [openCreateAccount, setOpenCreateAccount] = useState<boolean>(false)
  const [anchorEl, setAnchorEl] = useState(null)
  const classes = useStyles()
  const open = Boolean(anchorEl)
  const navigate = useNavigate()
  const [searchParams] = useSearchParams()

  useEffect(() => {
    const accountType = searchParams.get("account-type")
    if (accountType === "1") setCheckedBank(true)
    if (accountType === "2") setCheckedCash(true)
  }, [searchParams])
  const { accounts, accountDeletes, setAccounts, setAccountDeletes }: any =
    useManageAccount()

  const [{ data: resListAccount, loading, error }, refetchDeleteAccount] =
    useAxios(
      {
        url: `/account/deleteAccount?ids=${accountDeletes.toString()}`,
        method: "DELETE"
      },
      { manual: true }
    )

  const handleClose = () => {
    setAnchorEl(null)
    if (checkedBank && !checkedCash) {
      navigate(`${PATH.LIST_ACCOUNT}?account-type=1`)
    } else if (!checkedBank && checkedCash) {
      navigate(`${PATH.LIST_ACCOUNT}?account-type=2`)
    } else navigate(`${PATH.LIST_ACCOUNT}`)
  }

  const handleClickBank = () => {
    setCheckedBank(!checkedBank)
    if (!checkedBank) {
      setCheckedCash(false)
    }
  }

  const handleClickCash = () => {
    setCheckedCash(!checkedCash)
    if (!checkedCash) {
      setCheckedBank(false)
    }
  }

  const handleClick = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleDelete = () => {
    setOpenPopup(!openPopup)
    const finalData = accounts?.filter(
      item => !accountDeletes.includes(item?.account_id)
    )

    refetchDeleteAccount()
      .then(() => {
        setAccounts(finalData)
        setAccountDeletes([])
        notification({
          message: MESSAGE_DELETE_ACCOUNT,
          type: "success"
        })
      })
      .catch(() =>
        notification({
          message: MESSAGE_DELETE_ACCOUNT_FAIL,
          type: "error"
        })
      )
  }

  return (
    <>
      <PopupConfirm
        open={openPopup}
        onSubmit={handleDelete}
        type="warning"
        onClose={() => setOpenPopup(!openPopup)}
        title="Thông báo"
        text1="Xóa tài khoản"
        text2="Tài khoản bị xóa sẽ chuyển vào mục Lưu trữ. 
        Bạn có chắc chắn xóa tài khoản?"
      />
      <CreateAccount
        isOpen={openCreateAccount}
        handleClose={() => setOpenCreateAccount(false)}
      />
      <HeaderContent
        actions={[
          <Button
            onClick={() => setOpenCreateAccount(true)}
            className={classes.buttonAction}
            variant="contained"
            startIcon={<AddIcon />}
          >
            Thêm tài khoản
          </Button>,
          <Button
            className={classes.buttonAction}
            variant="contained"
            startIcon={<DeleteOutlineIcon />}
            onClick={() => setOpenPopup(true)}
            disabled={!!!accountDeletes.length}
          >
            Xóa
          </Button>,
          <Box>
            <Button
              className={classes.buttonAction}
              aria-controls={open ? "demo-positioned-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleClick}
              variant="contained"
              startIcon={<FilterAltIcon />}
            >
              Lọc tài khoản
            </Button>
            <Paper>
              <Menu
                id="demo-positioned-menu"
                aria-labelledby="demo-positioned-button"
                anchorEl={anchorEl}
                open={open}
                onClose={() => handleClose()}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                sx={{ mt: 1 }}
              >
                <MenuItem sx={{ width: "248px" }}>Loại tài khoản</MenuItem>
                <Divider sx={{ mx: 2 }} />
                <MenuItem onClick={() => handleClickBank()}>
                  <Checkbox sx={{ p: "0 9px 0 0" }} checked={checkedBank} />
                  Ngân hàng
                </MenuItem>
                <MenuItem onClick={() => handleClickCash()}>
                  <Checkbox sx={{ p: "0 9px 0 0" }} checked={checkedCash} />
                  Tiền mặt
                </MenuItem>
              </Menu>
            </Paper>
          </Box>
        ]}
      />
    </>
  )
}
