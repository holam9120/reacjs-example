import { useState, useEffect } from "react"
import { Checkbox, TableCell, TableRow } from "@mui/material"
import { useStyles } from "../styles"

interface IAccountNoneProps {
  numberItem: number
  isHaveCheckbox?: boolean
}

export default function AccountNone({
  numberItem,
  isHaveCheckbox
}: IAccountNoneProps) {
  const classes = useStyles()
  const [dataDefault, setDataDefault] = useState<number[]>([])

  useEffect(() => {
    const results: number[] = []
    const lengthArray = isHaveCheckbox ? numberItem + 1 : numberItem
    for (let i = 0; i < lengthArray; i++) {
      results.push(i)
    }
    setDataDefault(results)
  }, [])

  return (
    <TableRow>
      {dataDefault?.map(item => (
        <TableCell
          key={item}
          className={classes.cellTable}
          component="th"
          scope="row"
        >
          ...
        </TableCell>
      ))}
      {isHaveCheckbox && (
        <TableCell className={classes.cellTable} padding="checkbox">
          <Checkbox disabled />
        </TableCell>
      )}
    </TableRow>
  )
}
