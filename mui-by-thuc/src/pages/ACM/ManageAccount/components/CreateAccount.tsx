import { useState } from "react"
import * as yup from "yup"
import { Box } from "@mui/material"
import { useForm } from "react-hook-form"
import useAxios from "axios-hooks"
import { yupResolver } from "@hookform/resolvers/yup"
import { notification } from "src/utils/notification"
import PopupContent from "src/components/PopupContent"
import Input from "src/components/Input"
import Dropdown from "src/components/Dropdown"
import { useManageAccount } from "../index"

interface ICreateAccountProps {
  isOpen: boolean
  handleClose?: Function
  handleSubmit?: Function
}

export default function CreateAccount({
  isOpen,
  handleClose,
  handleSubmit
}: ICreateAccountProps) {
  const [accountName, setAccountName] = useState("")
  const [accountType, setAccountType] = useState("Chọn loại tiền mặt")
  const [bank, setBank] = useState("")
  const [accountNumber, setAccountNumber] = useState("")
  const [surplus, setSurplus] = useState("")
  const { accounts, setAccounts, refetchListAccount }: any = useManageAccount()
  const validateRequired = "Thông tin này không được để trống"

  const [{ data: resListAccount, loading, error }, refetchCreateAccount] =
    useAxios(
      {
        url: "/account/createAccount",
        method: "POST"
      },
      { manual: true }
    )

  const schema = yup.object().shape({
    accountName: yup.string().required(validateRequired),
    bank: yup.string().required(validateRequired),
    surplus: yup.string().required(validateRequired)
  })

  const {
    register,
    formState: { errors }
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema)
  })

  const handleAccountName = event => {
    register("accountName").onChange(event)
    setAccountName(event.target.value)
  }

  const handleAccountType = (option: string) => {
    setAccountType(option)
  }

  const handleBank = event => {
    register("bank").onChange(event)
    setBank(event.target.value)
  }

  const handleAccountNumber = event => {
    register("accountNumber").onChange(event)
    setAccountNumber(event.target.value)
  }

  const handleSurplus = event => {
    register("surplus").onChange(event)
    setSurplus(event.target.value)
  }

  const submit = () => {
    const fd = new FormData()
    fd.append("account_name", accountName)
    fd.append("account_number", accountNumber)
    fd.append("account", bank)
    fd.append("account_type", "1")
    fd.append("account_balance", surplus)
    fd.append("branchId", "1")

    refetchCreateAccount({ data: fd })
      .then(
        () =>
          notification({
            message: "Tạo tài khoản thành công",
            type: "success"
          }),
        refetchListAccount()
        // setAccounts([{accountName, accountNumber}, ...accounts])
      )
      .catch(() =>
        notification({
          message: "Tạo tài khoản thất bại",
          type: "error"
        })
      )
    handleClose?.()
  }

  const formCreate = (
    <Box display="grid" gridTemplateColumns="repeat(12, 1fr)" columnGap={3}>
      <Box gridColumn="span 6">
        <Input
          name="accountName"
          registerInput={register}
          type="text"
          required
          label="Tên tài khoản"
          error={errors.accountName}
          onChange={handleAccountName}
        />
      </Box>
      <Box gridColumn="span 6">
        <Dropdown
          option={["Tiền mặt", "Ngân hàng"]}
          onChange={handleAccountType}
          label="Loại tài khoản"
          value={accountType}
          required
        />
      </Box>
      <Box gridColumn="span 6">
        <Input
          name="bank"
          registerInput={register}
          type="text"
          required
          label="Ngân hàng"
          error={errors.bank}
          onChange={handleBank}
        />
      </Box>
      <Box gridColumn="span 6">
        <Input
          name="accountNumber"
          registerInput={register}
          type="text"
          label="Số tài khoản"
          onChange={handleAccountNumber}
        />
      </Box>
      <Box gridColumn="span 6">
        <Input
          name="surplus"
          registerInput={register}
          type="text"
          required
          label="Số dư"
          error={errors.surplus}
          onChange={handleSurplus}
        />
      </Box>
    </Box>
  )

  return (
    <PopupContent
      open={isOpen}
      onClose={() => handleClose?.(false)}
      onSubmit={submit}
      title={"Tạo dự án"}
      children={formCreate}
      textSubmit="Đồng ý"
    />
  )
}
