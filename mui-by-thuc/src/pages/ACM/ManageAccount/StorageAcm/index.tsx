import React from "react"
import HeaderStorageAcm from "../components/HeaderStorageAcm"
import ManageAccountProvider from "../index"

export default function StorageAcm() {
  const behindUrl = "/storeAccount/listAccounts?branchId=1"
  return (
    <ManageAccountProvider
      url={behindUrl}
      childrenHeader={<HeaderStorageAcm />}
    />
  )
}
