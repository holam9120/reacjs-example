import React from "react"
import { useSearchParams } from "react-router-dom"
import HeaderAction from "../components/HeaderListAccount"
import ManageAccountProvider from "../index"

export default function ListAccount() {
  const [search] = useSearchParams()
  const behindUrl = search.get("account-type")
    ? `/account/listAccounts?branchId=1&account_type=${search.get(
      "account-type"
    )}`
    : "/account/listAccounts?branchId=1"
  return (
    <ManageAccountProvider url={behindUrl} childrenHeader={<HeaderAction />} />
  )
}
