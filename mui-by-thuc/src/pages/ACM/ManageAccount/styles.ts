import { makeStyles } from "@material-ui/styles"
import { PRIMARY } from "src/constants/colors"

export const useStyles = makeStyles({
  headerContent: {
    background: PRIMARY,
    borderRadius: "12px 12px 0px 0px",
    padding: "10px 16px",
    color: "#ffffff"
  },
  buttonAction: {
    fontSize: 12,
    px: "12px"
  },
  cellTable: {
    border: "1px solid #ECECEC"
  },
  cellTableHeader: {
    border: "1px solid #ECECEC",
    background: "#E8E8E8"
  }
})
