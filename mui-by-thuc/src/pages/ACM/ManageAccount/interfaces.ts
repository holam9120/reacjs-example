import { Dispatch } from "react"

export interface ListAccountInterface {
  account_id: string
  account_name: string
  account_number: string
  account: string
  account_type: string
  workplaceId: string
  account_status: string
  balance: string
}

export interface Action {
  type: string
  payload: any
}

export interface IContextProps {
  state: StateType
  dispatch: Dispatch<Action>
}

export interface StateType {
  accounts: ListAccountInterface[]
}
