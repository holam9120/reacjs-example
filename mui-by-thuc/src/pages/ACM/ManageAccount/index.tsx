import {
  useEffect,
  createContext,
  useContext,
  useState,
  ReactNode,
  useCallback
} from "react"
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Checkbox
} from "@mui/material"
import useAxios from "axios-hooks"
import { ListAccountInterface } from "./interfaces"
import AccountNone from "./components/AccountNone"
import { useStyles } from "./styles"

const ManageAccountContext = createContext({})
ManageAccountContext.displayName = "AccountContext"

export const useManageAccount = () => {
  const context = useContext(ManageAccountContext)
  if (!context) throw new Error("Project Context Error")
  return context
}

interface IManageProps {
  childrenHeader?: ReactNode
  url: string
}

export default function ManageAccount({ childrenHeader, url }: IManageProps) {
  const [accounts, setAccounts] = useState<ListAccountInterface[]>([])
  const [checked, setChecked] = useState<boolean[]>([])
  const [accountDeletes, setAccountDeletes] = useState<string[]>([])
  const [checkedMulti, setCheckedMulti] = useState<boolean>(false)
  const classes = useStyles()

  const [{ data: resListAccount, loading, error }, refetchListAccount] =
    useAxios({
      url: url,
      method: "GET"
    })

  useEffect(() => {
    console.log("vao day")
    if (resListAccount?.data) {
      console.log("day cơ")

      setAccounts(resListAccount?.data)
      setChecked(resListAccount?.data.map(() => false))
    }
  }, [loading, resListAccount])

  const headerData: string[] = [
    "STT",
    "Tên tài khoản",
    "Loại tài khoản",
    "Ngân hàng",
    "Số tài khoản",
    "Số dư"
  ]

  const manageAccountState = useCallback(() => {
    return {
      accounts: resListAccount?.data || [],
      accountDeletes: accountDeletes || [],
      setAccounts,
      setAccountDeletes,
      refetchListAccount
    }
  }, [accountDeletes, refetchListAccount, resListAccount?.data])

  const handleCheckbox = (
    index: number,
    accountDelete: ListAccountInterface
  ) => {
    const finalDataChecked = checked.map((item, position) => {
      if (position === index) return !item
      return item
    })
    setChecked(finalDataChecked)

    const finalDataDelete = checked[index]
      ? accountDeletes.filter(item => item !== accountDelete.account_id)
      : [...accountDeletes, accountDelete.account_id]

    setChecked(finalDataChecked)
    setAccountDeletes(finalDataDelete)
  }

  const handleMultiCheckbox = () => {
    setChecked(checked.map(() => !checkedMulti))
    setCheckedMulti(!checkedMulti)
    checkedMulti
      ? setAccountDeletes([])
      : setAccountDeletes(accounts.map(item => item.account_id))
  }

  return (
    <ManageAccountContext.Provider value={manageAccountState()}>
      <Box>
        {childrenHeader}
        <Box className={classes.headerContent}>
          <p>Quản lý tài khoản</p>
        </Box>
        <TableContainer component={Paper}>
          <Table aria-label="simple table" size="small">
            <TableHead>
              <TableRow>
                {headerData.map((item, index) => (
                  <TableCell key={index} className={classes.cellTableHeader}>
                    {item}
                  </TableCell>
                ))}
                <TableCell
                  className={classes.cellTableHeader}
                  padding="checkbox"
                >
                  <Checkbox
                    checked={checkedMulti}
                    onChange={() => handleMultiCheckbox()}
                    disabled={!!!accounts.length}
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {accounts.length > 0 ? (
                accounts?.map((row, index: number) => (
                  <TableRow key={row?.account_id}>
                    <TableCell
                      className={classes.cellTable}
                      component="th"
                      scope="row"
                    >
                      {row.account_id}
                    </TableCell>
                    <TableCell
                      className={classes.cellTable}
                      component="th"
                      scope="row"
                    >
                      {row.account_name}
                    </TableCell>
                    <TableCell className={classes.cellTable}>
                      {row.account_type}
                    </TableCell>
                    <TableCell className={classes.cellTable}>
                      {row.account}
                    </TableCell>
                    <TableCell className={classes.cellTable}>
                      {row.account_number}
                    </TableCell>
                    <TableCell className={classes.cellTable}>
                      {row.balance}
                    </TableCell>
                    <TableCell className={classes.cellTable} padding="checkbox">
                      <Checkbox
                        checked={checked[index]}
                        onChange={() => handleCheckbox(index, row)}
                      />
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <AccountNone numberItem={5} isHaveCheckbox />
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </ManageAccountContext.Provider>
  )
}
