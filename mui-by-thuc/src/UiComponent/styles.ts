import styled from "styled-components"
import { DARK } from "../constants/colors"

export const StyledLoadingWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${DARK};
  opacity: 0.5;
  z-index: 10;
`
export const StyledLoadingContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;
  position: absolute;
`

export const LoaderWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
`
