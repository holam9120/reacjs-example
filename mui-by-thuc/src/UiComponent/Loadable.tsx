import { Suspense } from "react"
import { LinearProgress } from "@mui/material"
import { LoaderWrapper } from "./styles"

const Loader = () => (
  <LoaderWrapper>
    <LinearProgress color="primary" />
  </LoaderWrapper>
)

const Loadable = Component => props => {
  return (
    <Suspense fallback={<Loader />}>
      <Component {...props} />
    </Suspense>
  )
}

export default Loadable
