import { CircularProgress } from "@mui/material"
import { StyledLoadingWrapper, StyledLoadingContent } from "./styles"

export default function PageLoader() {
  return (
    <StyledLoadingWrapper>
      <StyledLoadingContent>
        <CircularProgress />
      </StyledLoadingContent>
    </StyledLoadingWrapper>
  )
}
