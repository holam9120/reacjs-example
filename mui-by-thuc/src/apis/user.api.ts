import request from "../utils/request"
import { getRefreshToken } from "../utils/auth"

export const postLogin = ({ username, password }: ReqLogin) => {
  return request({
    url: "/auth/login",
    method: "post",
    data: { username, password }
  })
}

export const postLogout = () => {
  return request({
    url: "/authentication/logout",
    method: "post",
    data: {
      refreshToken: getRefreshToken()
    }
  })
}
