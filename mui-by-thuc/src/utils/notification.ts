import { toast } from "react-toastify"
import { STATUS_SUCCESS } from "src/constants"
interface PropTypes {
  res?: any
  message?: string
  type?: "success" | "warning" | "error" | undefined
  displayTime?: number
}
const notification = ({
  res,
  message,
  type,
  displayTime = 2000
}: PropTypes) => {
  if (!res && !message) return
  return toast(message || res?.message || res?.data.message, {
    type:
      type ||
      (res?.status === 200
        ? STATUS_SUCCESS.includes(+res?.data?.code)
          ? "success"
          : "error"
        : STATUS_SUCCESS.includes(+res?.status)
        ? "success"
        : "error"),
    position: "top-right",
    autoClose: displayTime,
    hideProgressBar: true,
    pauseOnHover: false,
    closeButton: false
  })
}

export { notification }
