import React from "react"
import { configure } from "axios-hooks"
import LRU from "lru-cache"
import request from "../utils/request"
import { BrowserRouter } from "react-router-dom"
import Routes from "../router"
import "../assets/css/index.css"

const App = () => {
  const cache = new LRU({ max: 10 })
  configure({ axios: request, cache })

  return (
    // <BrowserRouter>
    <Routes />
    // </BrowserRouter>
  )
}

export default App
