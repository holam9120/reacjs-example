import { createTheme } from "@material-ui/core"
import defaultTheme from "./default"
import typography from "./typography"

const themes = {
  default: createTheme({ ...defaultTheme, typography: { ...typography } })
}

export default themes
