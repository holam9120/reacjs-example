import { SvgIcon } from "@mui/material"

const VerticalIcon = props => {
  return (
    <SvgIcon sx={{ pl: "20px" }} {...props}>
      <svg
        width="2"
        height="24"
        viewBox="0 0 2 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="2" height="24" fill="white" />
      </svg>
    </SvgIcon>
  )
}
export default VerticalIcon
