import useAxios from "axios-hooks"
import React, { createContext, useContext } from "react"
import { Outlet } from "react-router-dom"

const AcmContext = createContext({})

AcmContext.displayName = "AcmContext"

export const useProjects = () => {
  const context = useContext(AcmContext)
  if (!context) throw new Error("Project Context Error")
  return context
}

const defaultData = {
  staffId: 35,
  workplaceId: 1
}

export default function Acm(props) {
  const fd = new FormData()
  for (var key in defaultData) {
    fd.append(key, defaultData[key])
  }

  const [{ data }, refetchListProject] = useAxios({
    url: "/project/listProjects",
    method: "POST",
    data: fd
  })

  const value = {
    listProjects: data?.data,
    refetchListProject
  }
  return (
    <AcmContext.Provider value={value}>
      <Outlet />
    </AcmContext.Provider>
  )
}
