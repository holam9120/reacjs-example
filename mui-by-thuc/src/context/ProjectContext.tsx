import useAxios from "axios-hooks"
import React, { createContext, useContext } from "react"
import { Outlet } from "react-router-dom"

const ProjectContext = createContext({})

ProjectContext.displayName = "ProjectContext"

export const useProjects = () => {
  const context = useContext(ProjectContext)
  if (!context) throw new Error("Project Context Error")
  return context
}

const defaultData = {
  staffId: 35,
  workplaceId: 1
}

export default function Project(props) {
  const fd = new FormData()
  for (var key in defaultData) {
    fd.append(key, defaultData[key])
  }

  const [{ data }, refetchListProject] = useAxios({
    url: "/project/listProjects",
    method: "POST",
    data: fd
  })

  const value = {
    listProjects: data?.data,
    refetchListProject
  }
  return (
    <ProjectContext.Provider value={value}>
      <Outlet />
    </ProjectContext.Provider>
  )
}
