import { Dispatch } from "react"

export interface Action {
  type: string
  payload: any
}

export interface IContextProps {
  state: StateType
  dispatch: Dispatch<Action>
}

export interface StateType {
  isAuthenticated: boolean
}
