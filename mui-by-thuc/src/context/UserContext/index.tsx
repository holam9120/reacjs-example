import React, { useReducer, Reducer } from "react"
import UserStateContext from "./context"
import { userReducer } from "./reducer"
import { StateType, Action } from "./interfaces"
import { getToken } from "../../utils/auth"

const UserProvider = ({ children }) => {
  const [state, dispatch] = useReducer<Reducer<StateType, Action>>(
    userReducer,
    {
      isAuthenticated: !!getToken()
    }
  )

  const providerState = {
    state,
    dispatch
  }

  return (
    <UserStateContext.Provider value={providerState}>
      {children}
    </UserStateContext.Provider>
  )
}

export default UserProvider
