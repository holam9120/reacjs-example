import { createContext, useContext } from "react"
import { IContextProps } from "./interfaces"

const UserStateContext = createContext({} as IContextProps)

export const useCustomContext = () => {
  const context = useContext(UserStateContext)
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider")
  }
  return context
}

export default UserStateContext
