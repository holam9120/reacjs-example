import * as types from "./constants"
import { StateType, Action } from "./interfaces"
import { setToken } from "../../utils/auth"

export const userReducer = (state: StateType, action: Action) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      setToken(action.payload)
      return { ...state, isAuthenticated: true }
    case types.SIGN_OUT_SUCCESS:
      return { ...state, isAuthenticated: false }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}
