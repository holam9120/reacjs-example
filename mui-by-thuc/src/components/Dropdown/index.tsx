import { useState } from "react"
import { ReactComponent as ArrowDropIcon } from "src/assets/icons/arrow-drop.svg"
import { useStyles } from "./styles"

interface DropdownProps {
  value: string
  label?: string
  required?: boolean
  option: string[]
  placeholder?: string
  errorMessage?: string
  error?: string
  className?: string
  disabled?: boolean
  onClick?: Function
  onChange(value: string): void
}

export default function Dropdown({
  value,
  label,
  required = false,
  option,
  errorMessage,
  error = "",
  disabled = false,
  onClick = Function,
  onChange
}: DropdownProps) {
  const [isOpen, setIsOpen] = useState(false)
  const classes = useStyles()

  const selectItem = (event: string) => {
    setIsOpen(!isOpen)
    onChange(event)
  }
  const handleClick = () => {
    setIsOpen(!isOpen)
    onClick()
  }

  return (
    <div>
      <label>
        {label} {required && <span className={classes.spanRequired}>*</span>}
      </label>
      <div className={classes.dropdownContent}>
        <div
          className={`${classes.dropdownSelect} ${(errorMessage || error) && classes.dropdownSelectError
            }`}
          onClick={() => handleClick()}
        >
          <span>{value}</span>
          <span className={classes.dropdownIcon}>
            <ArrowDropIcon />
          </span>
        </div>
        <p className={classes.dropDownError}>{error ? error : ""}</p>

        {isOpen && (
          <div className={classes.dropdownList}>
            {option.map((item, key) => (
              <div
                key={key}
                onClick={() => selectItem(item)}
                className={classes.dropdownListItem}
              >
                {item}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  )
}
