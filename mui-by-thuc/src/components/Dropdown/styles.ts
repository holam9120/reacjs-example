import { makeStyles } from "@material-ui/styles"
import { DARK_90, ERROR, WHITE, WHITE_10, PRIMARY } from "src/constants/colors"

export const useStyles = makeStyles({
  dropdownTitle: {
    margin: "5px 0",
    width: "100%"
  },
  spanRequired: {
    marginLeft: "6px",
    color: ERROR
  },
  dropdownContent: {
    position: "relative"
  },
  dropdownSelect: {
    position: "relative",
    border: "1px solid #262626",
    borderRadius: "8px",
    padding: "7px",
    margin: "4px 0"
  },
  dropdownSelectError: {
    border: `1px solid ${ERROR}`
  },
  dropdownIcon: {
    position: "absolute",
    right: "9px",
    top: "0",
    height: "100%",
    display: "flex",
    alignItems: "center",
    cursor: "pointer"
  },
  dropDownError: {
    color: ERROR,
    height: "20px"
  },
  dropdownList: {
    position: "absolute",
    width: "100%",
    maxHeight: "200px",
    borderRadius: "8px",
    overflow: "auto",
    top: "50px",
    left: "0",
    zIndex: 2,
    border: `1px solid ${DARK_90}`,
    backgroundColor: WHITE
  },
  dropdownListItem: {
    width: "100%",
    padding: "9px 7px",
    boxSizing: "border-box",
    cursor: "pointer",
    "&:hover": {
      color: PRIMARY,
      backgroundColor: WHITE_10
    }
  }
})
