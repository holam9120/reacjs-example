# Example:

```js
interface InputProps {
  label?: string
  required?: boolean
  icon?: ReactNode
  name: string
  registerInput: any
  type?: "button" | "date" | "password" | "submit" | "text"
  placeholder?: string
  errorMessages?: string
  error?: any
  disabled?: boolean
  onChange: FormEventHandler<HTMLInputElement>
}

  const [accountName, setAccountName] = useState("")
  const schema = yup.object().shape({
    accountName: yup.string().required("Tên tài khoản không được để trống"),
  })

  const handleAccountName = event => {
    register("accountName").onChange(event)
    setAccountName(event.target.value)
  }

  const {
    register,
    formState: { errors }
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema)
  })

  <Input
    name="accountName"
    registerInput={register}
    type="text"
    required={true}
    label="Tài khoản"
    error={errors.accountName}
    onChange={handleAccountName}
    icon={<UploadIcon />}
  />

```

# Show more props on [Modal](https://mui.com/api/modal/)
