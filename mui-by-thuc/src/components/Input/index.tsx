import React, { FormEventHandler, ReactNode } from "react"
import { useStyles } from "./styles"

interface InputProps {
  label?: string
  required?: boolean
  icon?: ReactNode
  name: string
  registerInput: any
  type?: "button" | "date" | "password" | "submit" | "text"
  placeholder?: string
  errorMessages?: string
  error?: any
  disabled?: boolean
  onChange: FormEventHandler<HTMLInputElement>
}

export default function Input({
  label = "",
  required = false,
  icon,
  name,
  registerInput,
  type = "text",
  placeholder,
  errorMessages,
  error = "",
  disabled = false,
  onChange
}: InputProps) {
  const classes = useStyles()

  const handleChange = (value: React.ChangeEvent<HTMLInputElement>) => {
    onChange(value)
  }

  return (
    <div>
      <label htmlFor={name}>
        {label} {required && <span className={classes.spanRequired}>*</span>}
      </label>
      <div className={classes.inputContent}>
        <input
          type={type}
          id={name}
          placeholder={placeholder}
          disabled={disabled}
          {...registerInput(name)}
          onChange={handleChange}
          className={`${classes.input} ${(errorMessages || error) && classes.inputError
            }`}
        />
        <span
          className={`${classes.spanIcon}  ${(errorMessages || error) && classes.iconError
            }`}
        >
          {icon}
        </span>
      </div>
      <p className={classes.error}>{error ? error.message : ""}</p>
    </div>
  )
}
