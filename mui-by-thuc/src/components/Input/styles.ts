import { makeStyles } from "@material-ui/styles"
import { DARK_90, ERROR } from "src/constants/colors"

export const useStyles = makeStyles({
  spanRequired: {
    marginLeft: "6px",
    color: ERROR
  },
  inputContent: {
    position: "relative"
  },
  input: {
    margin: "4px 0",
    width: "100%",
    padding: "8px",
    border: `1px solid ${DARK_90}`,
    borderRadius: "8px",
    outline: "none",
    boxSizing: "border-box"
  },
  inputError: {
    border: `1px solid ${ERROR}`
  },
  spanIcon: {
    position: "absolute",
    right: "9px",
    top: "0",
    height: "100%",
    display: "flex",
    alignItems: "center",
    cursor: "pointer"
  },
  iconError: {
    "& path": {
      fill: ERROR
    }
  },
  error: {
    color: ERROR,
    height: "20px"
  }
})
