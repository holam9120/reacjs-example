import { Box, Grid, Typography } from "@mui/material"

interface IHeaderContentProps {
  title?: string | any
  description?: string | any
  actions?: [] | any
}
export default function HeaderContent({
  title,
  description,
  actions
}: IHeaderContentProps) {
  return (
    <Box my={2}>
      <Grid container justifyContent="space-between">
        <Grid item flexDirection={"column"}>
          <Typography variant="h6"> {title} </Typography>
          <Typography variant="body2"> {description} </Typography>
        </Grid>
        <Grid item alignSelf={"center"}>
          <Grid container spacing={2}>
            {actions?.length &&
              actions?.map((o, i) => (
                <Grid item key={i}>
                  {o}
                </Grid>
              ))}
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}
