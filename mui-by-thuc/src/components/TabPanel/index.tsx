import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import Tabs from "@mui/material/Tabs"
import * as React from "react"
import { useStyles } from "./styles"

interface TabPanelItemsProps {
  title: string
  index: number
}
interface TabPanelProps {
  children?: React.ReactNode | any
  items: TabPanelItemsProps[]
  setValue?: any
  value?: number
}

function TabPanel({ children, items, value, setValue }: TabPanelProps) {
  const classes = useStyles()
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    if (setValue) {
      setValue(newValue)
    }
  }
  return (
    <Box sx={{ padding: 3 }}>
      <Tabs
        value={value}
        onChange={handleChange}
        TabIndicatorProps={
          children?.length === 1
            ? undefined
            : {
              children: <span className="MuiTabs-indicatorSpan" />
            }
        }
        className={classes.tab}
      >
        {items.map((o, i) => (
          <Tab label={o?.title} className={classes.tabPanel} key={i} />
        ))}
      </Tabs>
      {children}
    </Box>
  )
}

export default TabPanel
