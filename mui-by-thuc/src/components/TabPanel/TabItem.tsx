import React from "react"
import { Box, Typography } from "@mui/material"
import { useStyles } from "./styles"

interface TabItemProps {
  children?: React.ReactNode
  index: number
  value: number
}

const TabItem = ({ children, value, index, ...other }: TabItemProps) => {
  const classes = useStyles()
  return (
    <div
      hidden={value !== index}
      id={`simple-TabItem-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      className={classes.tabItem}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

export default TabItem
