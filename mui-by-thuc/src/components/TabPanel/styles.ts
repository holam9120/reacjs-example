import { makeStyles } from "@material-ui/styles"
import { PRIMARY } from "src/constants/colors"

export const useStyles = makeStyles({
  tab: {
    backgroundColor: PRIMARY,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    border: "1px solid #ccc",
    "& .MuiTabs-indicator": {
      display: "flex",
      justifyContent: "center",
      backgroundColor: "transparent"
    },
    "& .MuiTabs-indicatorSpan": {
      maxWidth: 40,
      width: "100%",
      backgroundColor: "#fff"
    }
  },
  tabPanel: {
    textTransform: "none",
    color: "#fff",
    "&.Mui-selected": {
      color: "#fff"
    }
  },
  tabItem: {
    backgroundColor: "#F5F6F7"
  }
})
