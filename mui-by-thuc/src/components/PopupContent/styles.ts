import { makeStyles } from "@material-ui/styles"
import { PRIMARY, WHITE } from "src/constants/colors"

export const useStyles = makeStyles({
  modal: {
    position: "absolute" as "absolute",
    top: "50%",
    left: "calc(50% + 122px)",
    transform: "translate(-50%, -50%)",
    width: "70%",
    lineHeight: "20px",
    "&:focus": {
      outline: "none"
    }
  },
  modalTitle: {
    minHeight: "30px",
    textAlign: "center",
    color: WHITE,
    backgroundColor: PRIMARY,
    padding: "15px",
    borderRadius: "10px 10px 0 0"
  },
  modalContent: {
    backgroundColor: WHITE,
    padding: "24px",
    maxHeight: "80vh",
    overflow: "auto"
  },

  footer: {
    backgroundColor: WHITE,
    borderRadius: "0 0 10px 10px",
    "& .MuiButton-root": {
      margin: "10px",
      width: "116px",
      borderRadius: "4px"
    }
  }
})
