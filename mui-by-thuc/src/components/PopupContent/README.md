# Example:

```js
interface IPopupContentProps {
  open: boolean
  title?: string
  onSubmit?: void | any
  onClose?: () => void
  textSubmit?: string
  textCancel?: string
  children?: React.ReactNode
  footer?: React.ReactNode
  hiddenFooter?: boolean
  props?: any
}

 const formCreate = (
    <Grid container justifyItems={"flex-start"} spacing={2}>
      <Grid item xs={6}>
        <TextField fullWidth />
      </Grid>
      <Grid item xs={6}>
        <TextField fullWidth />
      </Grid>
    </Grid>
  )

 <PopupContent
          open={visibleFormCreate}
          onClose={() => setVisibleFormCreate(false)}
          title={"Tạo dự án"}
          children={formCreate}
          textSubmit="Đồng ý"
          // hiddenFooter={true}
        />

```

# Show more props on [Modal](https://mui.com/api/modal/)
