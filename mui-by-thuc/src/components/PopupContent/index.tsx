import { Grid } from "@mui/material"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import Modal from "@mui/material/Modal"
import Typography from "@mui/material/Typography"
import * as React from "react"
import { useStyles } from "./styles"

interface IPopupContentProps {
  open: boolean
  title?: string
  onSubmit?: void | any
  onClose?: () => void
  textSubmit?: string
  textCancel?: string
  children?: React.ReactNode
  footer?: React.ReactNode
  hiddenFooter?: boolean
  props?: any
}

export default function PopupContent(props) {
  const {
    children,
    open,
    title,
    onSubmit,
    onClose,
    textSubmit,
    textCancel,
    hiddenFooter,
    footer
  }: IPopupContentProps = props

  const classes = useStyles()

  const footerContent = hiddenFooter ? null : footer ? (
    footer
  ) : (
    <Grid container item alignSelf={"center"} justifyContent="center" m={2}>
      <Button variant="outlined" onClick={onClose}>
        {textCancel || "Huỷ"}
      </Button>
      <Button variant="contained" onClick={() => onSubmit?.()}>
        {textSubmit || "Lưu"}
      </Button>
    </Grid>
  )

  return (
    <Modal open={open} onClose={onClose} {...props}>
      <Box className={classes.modal}>
        <Typography
          className={classes.modalTitle}
          variant="subtitle1"
          component="h2"
        >
          {title}
        </Typography>
        <Grid container flexDirection={"column"} justifyContent="space-between">
          <Grid item>
            <div className={classes.modalContent}>{children}</div>
          </Grid>
          {footerContent && (
            <Grid item container className={classes.footer}>
              {footerContent}
            </Grid>
          )}
        </Grid>
      </Box>
    </Modal>
  )
}
