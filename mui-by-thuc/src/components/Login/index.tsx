import { useState, FormEvent } from "react"
import { useNavigate } from "react-router-dom"
import useAxios from "axios-hooks"
import Loading from "../../UiComponent/Loading"
import { PATH } from "../../constants/paths"
import { useCustomContext } from "../../context/UserContext/context"
import * as types from "../../context/UserContext/constants"
import { StyledLabel, StyledInput, StyledButton } from "./styles"

const Login = () => {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const { dispatch } = useCustomContext()
  const navigate = useNavigate()

  const [{ loading, error }, refetchListProj] = useAxios(
    {
      url: "/auth/login",
      method: "POST"
    },
    { manual: true }
  )

  const onSubmit = async (event: FormEvent) => {
    event.preventDefault()

    const fd = new FormData()
    fd.append("username", username)
    fd.append("password", password)

    refetchListProj({ data: fd })
      .then(res => {
        dispatch({
          type: types.LOGIN_SUCCESS,
          payload: res.data.data.token
        })
        navigate(PATH.HOME)
      })
      .catch(res => console.log("res", res))
  }

  return (
    <div className="login">
      <h1>Login</h1>
      <form onSubmit={onSubmit}>
        <div>
          <StyledLabel>Username</StyledLabel>
          <StyledInput
            type="text"
            value={username}
            placeholder="Enter Username"
            onChange={e => setUsername(e.target.value)}
          />
        </div>
        <div>
          <StyledLabel>Password</StyledLabel>
          <StyledInput
            type="password"
            value={password}
            placeholder="Enter Password"
            onChange={e => setPassword(e.target.value)}
          />
        </div>
        <p>{error}</p>
        <StyledButton type="submit">Submit</StyledButton>
      </form>
      {loading && <Loading />}
    </div>
  )
}
export default Login
