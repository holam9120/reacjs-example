import styled from "styled-components"

export const StyledLabel = styled.label`
  margin-bottom: 10px;
  display: block;
  font-size: 15px;
`
export const StyledInput = styled.input`
  width: 100%;
  padding: 10px;
  border-radius: 10px;
  outline: none;
  border: 1px solid #ccc;
  font-size: 12px;
`
export const StyledButton = styled.button`
  font-size: 15px;
  margin-top: 15px;
  width: 100%;
  background: blue;
  border-radius: 10px;
  padding: 5px;
  color: white;
`
