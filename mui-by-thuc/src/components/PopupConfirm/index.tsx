import { Grid } from "@mui/material"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import Modal from "@mui/material/Modal"
import Typography from "@mui/material/Typography"
import * as React from "react"
import { ReactComponent as SuccessIcon } from "src/assets/icons/success.svg"
import { ReactComponent as WarningIcon } from "src/assets/icons/warning.svg"
import { useStyles } from "./styles"

interface IPopupConfirmProps {
  open: boolean
  title?: string | React.ReactNode | any
  text1?: string | React.ReactNode | any
  text2?: string | React.ReactNode | any
  onSubmit?: void | any
  onClose?: () => void
  icon?: React.ReactNode
  type?: "success" | "warning" | "error" | string
  textSubmit?: string
  textCancel?: string
  children?: React.ReactNode
  footer?: React.ReactNode
  hiddenFooter?: boolean
}

export default function PopupConfirm(props) {
  const {
    open,
    title,
    text1,
    text2,
    onSubmit,
    onClose,
    type,
    icon,
    children,
    textSubmit,
    textCancel,
    hiddenFooter,
    footer
  }: IPopupConfirmProps = props

  const classes = useStyles()

  const footerContent = hiddenFooter ? null : footer ? (
    footer
  ) : (
    <Grid container item alignSelf={"center"} justifyContent="center">
      <Button variant="outlined" onClick={onClose}>
        {textCancel || "Huỷ"}
      </Button>
      <Button variant="contained" onClick={() => onSubmit?.()}>
        {textSubmit || "Xác nhận"}
      </Button>
    </Grid>
  )

  const renderIcon = () => {
    switch (type) {
      case "success":
        return <SuccessIcon />
      case "warning":
        return <WarningIcon />
      default:
        break
    }
  }

  const content = () => {
    if (children) return <div className={classes.modalContent}>{children}</div>

    return (
      <div className={classes.modalContent}>
        {icon ? icon : renderIcon()}
        <div className={classes.modalContentTitle}>{text1}</div>
        <div className={classes.modalContentText}>{text2}</div>
      </div>
    )
  }
  return (
    <Modal open={open} onClose={onClose} {...props}>
      <Box className={classes.modal}>
        <Typography
          className={classes.modalTitle}
          variant="subtitle1"
          component="h2"
        >
          {title || "Thông báo"}
        </Typography>
        {content()}
        <Grid item container className={classes.footer}>
          {footerContent}
        </Grid>
      </Box>
    </Modal>
  )
}
