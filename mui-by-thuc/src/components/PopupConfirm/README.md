# Example:

```js
interface IPopupConfirmProps {
  open: boolean
  title?: string | React.ReactNode | any
  text1?: string | React.ReactNode | any
  text2?: string | React.ReactNode | any
  onSubmit?: void | any
  onClose?: () => void
  icon?: React.ReactNode
  type?: "success" | "warning" | "error" | string
  textSubmit?: string
  textCancel?: string
  children?: React.ReactNode
  footer?: React.ReactNode
  hiddenFooter?: boolean
}

  <PopupConfirm
          open={true}
          type='success'
          onClose={onClose}
          title="Xoá dự án"
          text1="Dự án x"
          text2="Dự án thành viên"
        />

```

# Show more props on [Modal](https://mui.com/api/modal/)
