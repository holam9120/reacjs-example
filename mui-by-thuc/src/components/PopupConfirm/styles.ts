import { makeStyles } from "@material-ui/styles"
import { PRIMARY, WHITE } from "src/constants/colors"

export const useStyles = makeStyles({
  modal: {
    position: "absolute" as "absolute",
    top: "50%",
    left: "calc(50% + 122px)",
    transform: "translate(-50%, -50%)",
    width: 488,
    fontSize: "16px",
    lineHeight: "20px",
    "&:focus": {
      outline: "none"
    }
  },
  modalTitle: {
    textAlign: "center",
    color: WHITE,
    backgroundColor: PRIMARY,
    padding: "10px",
    borderRadius: "10px 10px 0 0"
  },
  modalContent: {
    textAlign: "center",
    backgroundColor: WHITE,
    padding: "20px 90px"
  },
  modalContentTitle: {
    fontWeight: 700,
    marginTop: "15px"
  },
  modalContentText: {
    marginTop: "8px"
  },
  footer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: WHITE,
    borderRadius: "0 0 10px 10px",
    "& .MuiButton-root": {
      margin: "10px",
      width: "116px",
      borderRadius: "4px"
    }
  }
})
