export const REGEX_EMAIL = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{3,4}$"
export const MESSAGE_DELETE_ACCOUNT = "Xóa tài khoản thành công"
export const MESSAGE_DELETE_ACCOUNT_FAIL = "Xóa tài khoản thất bại"
export const STATUS_SUCCESS = [200]
