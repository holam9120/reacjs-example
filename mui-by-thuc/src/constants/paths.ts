export const PATH = {
  HOME: "/",
  WORK: "/work",
  PROJECT: "/work/project",
  PERSONAL: "/work/personal",
  STORAGE: "/work/storage",
  STATISTIC: "/work/statistic",
  SETTING: "/work/setting",
  ACM: "/acm",
  MANAGE_ACCOUNT: "/acm/manage-account",
  LIST_ACCOUNT: "/acm/list-account",
  STORAGE_ACM: "/acm/storage",
  REVENUE_EXPENDITURE: "/acm/manage-revenue-expenditure",
  REQUEST_PAYMENT_ADVANCE: "/acm/request-payment-advance",
  LOGIN: "/login"
}
