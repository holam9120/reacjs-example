import projects from "./projects"
import acm from "./acm"

export default { projects, acm }
