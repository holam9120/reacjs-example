import WorkIcon from "../../assets/icons/Work"
import VerticalIcon from "../../assets/icons/Vertical"
import SubMenuIcon from "../../assets/icons/SubMenu"

export default [
  {
    key: "1",
    title: "Công việc",
    url: "projects",
    icon: <WorkIcon />,
    children: [
      {
        key: "2",
        title: "Dự án",
        url: "/projects/list",
        icon: <SubMenuIcon />
      },
      {
        key: "3",
        title: "Cá nhân",
        url: "/projects/task-user",
        icon: <SubMenuIcon />
      },
      {
        key: "4",
        title: "Lưu trữ",
        url: "/projects/task-save",
        icon: <SubMenuIcon />
      },
      {
        key: "5",
        title: "Thống kê",
        url: "/projects",
        icon: <SubMenuIcon />,
        children: [
          {
            key: "6",
            title: "Cài đặt 01",
            url: "/projects/",
            icon: <VerticalIcon />
          },
          {
            key: "7",
            title: "Cài đặt 02",
            url: "/projects",
            icon: <VerticalIcon />
          }
        ]
      },
      {
        key: "6",
        title: "Cài đặt",
        url: "/projects/settings",
        icon: <SubMenuIcon />,
        children: [
          {
            key: "6",
            title: "Cài đặt 01",
            url: "/",
            icon: <VerticalIcon />
          },
          {
            key: "7",
            title: "Cài đặt 02",
            url: "/",
            icon: <VerticalIcon />
          }
        ]
      }
    ]
  }
]
