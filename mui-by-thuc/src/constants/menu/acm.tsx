/* eslint-disable import/no-anonymous-default-export */
import VerticalIcon from "../../assets/icons/Vertical"
import SubMenuIcon from "../../assets/icons/SubMenu"
import MoneyIcon from "../../assets/icons/Money"
import { PATH } from "../paths"

export default [
  {
    key: "1",
    title: "ACM",
    url: PATH.ACM,
    icon: <MoneyIcon />,
    children: [
      {
        key: "2",
        title: "Quản lý tài khoản",
        url: PATH.ACM,
        icon: <SubMenuIcon />,
        children: [
          {
            key: "3",
            title: "Danh sách tài khoản",
            url: PATH.LIST_ACCOUNT,
            icon: <VerticalIcon />
          },
          {
            key: "4",
            title: "Lưu trữ",
            url: PATH.STORAGE_ACM,
            icon: <VerticalIcon />
          }
        ]
      },
      {
        key: "5",
        title: "Quản lý thu chi",
        url: PATH.REVENUE_EXPENDITURE,
        icon: <SubMenuIcon />
      },
      {
        key: "6",
        title: "Đề nghị thanh toán/Tạm ứng",
        url: PATH.REQUEST_PAYMENT_ADVANCE,
        icon: <SubMenuIcon />
      }
    ]
  }
]
