import styled from "styled-components"

export const StyledMainLayout = styled.div`
  display: flex;
  min-height: 100vh;
`
export const StyledMainLayoutContent = styled.div`
  width: calc(100vw - 244px);
`
