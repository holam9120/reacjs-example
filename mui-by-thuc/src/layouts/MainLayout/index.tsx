import { Outlet } from "react-router-dom"
import { Box } from "@mui/material"
import Navigation from "./Navigation"
import Sidebar from "./Sidebar"
import { StyledMainLayout, StyledMainLayoutContent } from "./styles"

const MainLayout = () => {
  return (
    <StyledMainLayout>
      <Sidebar />
      <StyledMainLayoutContent>
        <Navigation />
        <Box mx={4}>
          <Outlet />
        </Box>
      </StyledMainLayoutContent>
    </StyledMainLayout>
  )
}
export default MainLayout
