import * as React from "react"
import { AppBar, Avatar, Box, Button, InputBase, Paper } from "@mui/material"
import { PRIMARY, PRIMARY_ACTIVE } from "../../../constants/colors"
import MENU from "../../../assets/images/menu.png"
import NOTIFICATION from "../../../assets/images/notification.png"
import SearchIcon from "../../../assets/icons/Search"
import { removeRefreshToken, removeToken } from "src/utils/auth"
import { useLocation, useNavigate } from "react-router-dom"

const pages = [
  {
    title: "Bảng tin",
    url: "/"
  },
  {
    title: "ToDo",
    url: "/projects"
  },
  {
    title: "ACM",
    url: "/acm"
  }

  // "Calendar",
  // "HRM",
  // "CRM",
  // "ACM",
  // "Ticket",
  // "Repo",
  // "Insight"
]

const Navigation = () => {
  const navigator = useNavigate()
  const location = useLocation()
  const str = location?.pathname?.split("/")?.[1]

  const doLogout = () => {
    removeToken()
    removeRefreshToken()
    navigator("/login")
  }

  const changeRoutes = o => {
    navigator(o?.url)
  }

  return (
    <AppBar
      position="static"
      sx={{
        height: 56,
        backgroundColor: PRIMARY,
        boxShadow: "none"
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between"
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          {pages.map(page => (
            <Button
              key={page?.url}
              sx={{
                p: "4px 8px",
                mr: "8px",
                color: "white",
                textTransform: "none",
                fontSize: "16px",
                backgroundColor:
                  str === page?.url?.split("/")?.[1] ? PRIMARY_ACTIVE : PRIMARY
              }}
              onClick={() => changeRoutes(page)}
            >
              {page?.title}
            </Button>
          ))}
        </Box>

        <Box
          sx={{
            display: "flex",
            py: "8px",
            pr: "32px"
          }}
        >
          <Paper
            component="form"
            sx={{
              my: "4px",
              display: "flex",
              alignItems: "center",
              width: 300,
              borderRadius: "8px"
            }}
          >
            <InputBase
              sx={{ ml: 1, flex: 1, fontSize: 12 }}
              placeholder="Nhập từ khóa"
              inputProps={{ "aria-label": "search" }}
            />
            <SearchIcon sx={{ p: "8px", fontSize: 16 }} />
          </Paper>
          <Avatar alt="Avatar" src="#" sx={{ ml: "32px" }} />
          <Avatar alt="Remy Sharp" src={NOTIFICATION} sx={{ mx: "16px" }} />
          <Avatar alt="Remy Sharp" src={MENU} />
          <Button variant="contained" onClick={() => doLogout()}>
            Logout
          </Button>
        </Box>
      </Box>
    </AppBar>
  )
}
export default Navigation
