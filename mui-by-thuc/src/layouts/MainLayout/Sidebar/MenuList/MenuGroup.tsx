import { ReactNode, useState } from "react"
import ExpandLess from "@mui/icons-material/ExpandLess"
import ExpandMore from "@mui/icons-material/ExpandMore"
import Collapse from "@mui/material/Collapse"
import List from "@mui/material/List"
import MenuItem from "../MenuList/MenuItem"
import { WHITE } from "../../../../constants/colors"
import { Menu } from "./interfaces"

interface Props {
  menuData: Menu
  children: ReactNode
}

const MenuList = ({ menuData, children }: Props) => {
  const [open, setOpen] = useState(true)

  return (
    <>
      <MenuItem menuData={menuData} handleClick={() => setOpen(!open)}>
        {open ? (
          <ExpandLess sx={{ color: WHITE }} />
        ) : (
          <ExpandMore sx={{ color: WHITE }} />
        )}
      </MenuItem>

      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {children}
        </List>
      </Collapse>
    </>
  )
}
export default MenuList
