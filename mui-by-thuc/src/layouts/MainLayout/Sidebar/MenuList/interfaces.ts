export interface Menu {
  key: string
  title: string
  url: string
  icon: JSX.Element
  children?: Menu[]
}
