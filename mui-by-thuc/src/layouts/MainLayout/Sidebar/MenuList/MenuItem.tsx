import { ReactNode } from "react"
import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material"
import { useLocation } from "react-router-dom"
import { WHITE, PRIMARY, PRIMARY_ACTIVE } from "../../../../constants/colors"
import { Menu } from "./interfaces"
import { StyledLink } from "../styles"

interface Props {
  menuData: Menu
  children?: ReactNode
  handleClick?: void | any
}

const MenuList = ({ menuData, children, handleClick }: Props) => {
  const location = useLocation().pathname
  return (
    <ListItemButton
      sx={{
        color: WHITE,
        backgroundColor: location === menuData.url ? PRIMARY_ACTIVE : PRIMARY
      }}
      onClick={() => handleClick?.()}
    >
      <ListItemIcon sx={{ color: WHITE, minWidth: "40px" }}>
        {menuData.icon}
      </ListItemIcon>
      <ListItemText
        sx={{ color: WHITE }}
        primary={<StyledLink to={menuData?.url}>{menuData.title}</StyledLink>}
      />
      {children}
    </ListItemButton>
  )
}

export default MenuList
