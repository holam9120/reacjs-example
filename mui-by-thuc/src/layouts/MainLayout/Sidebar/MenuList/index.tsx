import { List } from "@mui/material"
import MenuGroup from "./MenuGroup"
import MenuItem from "./MenuItem"
import { Menu } from "./interfaces"
import { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import listMenu from "src/constants/menu/listMenu"

const MenuList = () => {
  const location = useLocation()
  const [menus, setMenu] = useState<Menu[]>([])
  const str = location?.pathname?.split("/")?.[1] || "projects"
  const navigator = useNavigate()

  const getMenus = () => {
    const currentMenu = listMenu?.[str]
    setMenu(currentMenu)
  }

  const onClickMenuItem = e => {
    navigator(e?.url)
  }

  useEffect(() => {
    getMenus()
  }, [str])

  const renderMenuItem = (menu: Menu) => {
    return menu.children ? (
      <MenuGroup menuData={menu}>
        {menu?.children?.map((item: Menu) => (
          <div key={item.key}>{renderMenuItem(item)}</div>
        ))}
      </MenuGroup>
    ) : (
      <MenuItem menuData={menu} handleClick={() => onClickMenuItem(menu)} />
    )
  }

  return (
    <List
      sx={{ width: "100%", p: "0px" }}
      component="nav"
      aria-labelledby="nested-list-subheader"
    >
      {menus.map(menu => (
        <div key={menu.key}>{renderMenuItem(menu)}</div>
      ))}
    </List>
  )
}
export default MenuList
