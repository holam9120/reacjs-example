import styled from "styled-components"
import { Link } from "react-router-dom"
import { PRIMARY } from "../../../constants/colors"

export const StyledSidebar = styled.div`
  background-color: ${PRIMARY};
  width: 244px;
`
export const StyledImage = styled.img`
  padding: 8px 32px;
  display: block;
`
export const StyledLink = styled(Link)`
  color: WHITE;
  text-decoration: none;
  font-size: 14px;
`
