import MenuList from "./MenuList"
import LOGO from "../../../assets/images/logo.png"
import { StyledSidebar, StyledImage } from "./styles"

const Sidebar = () => {
  return (
    <StyledSidebar>
      <StyledImage src={LOGO} alt="logo" height={40} />
      <MenuList />
    </StyledSidebar>
  )
}
export default Sidebar
