import React, { ReactNode } from "react"
import { StyledAuthWrapper, StyledAuthLeft, StyledAuthRight } from "./styles"
interface Props {
  children: ReactNode
}

const AuthLayout = ({ children }: Props) => {
  return (
    <StyledAuthWrapper>
      <StyledAuthLeft></StyledAuthLeft>
      <StyledAuthRight>{children}</StyledAuthRight>
    </StyledAuthWrapper>
  )
}
export default AuthLayout
