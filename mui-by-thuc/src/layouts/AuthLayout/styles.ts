import styled from "styled-components"

export const StyledAuthWrapper = styled.div`
  display: flex;
  width: 1440px;
  margin: 0 auto;
  justify-content: center;
  align-items: center;
  height: 100vh;
`
export const StyledAuthLeft = styled.div`
  background-color: red;
  height: 100vh;
  width: calc(100vw - 576px);
`
export const StyledAuthRight = styled.div`
  width: 500px;
  height: 100vh;
  padding: 20px 30px;
  border-radius: 10px;
  border: 1px solid #ccc;
  box-shadow: 0 1px 2px #ccc;
`
