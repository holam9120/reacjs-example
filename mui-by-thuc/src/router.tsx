import { lazy } from "react"
// import { Navigate, useRoutes } from "react-router-dom"
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"
import AuthenticatedGuard from "./guards/AuthenticatedGuard"
import PublicGuard from "./guards/PublicGuard"
import { PATH } from "./constants/paths"
import MainLayout from "./layouts/MainLayout"
import Loading from "./UiComponent/Loading"
import Loadable from "./UiComponent/Loadable"
// import ProjectContext from "./context/ProjectContext"
// import Home from "./pages/Home"
// import TaskUser from "./pages/TaskUser"
// import ListTask from "./pages/ListTask"
// import AMC from "./pages/ACM"
// import NotFound from "./pages/NotFound"

const Login = Loadable(lazy(() => import("./pages/Login")))
const Home = Loadable(lazy(() => import("./pages/Home")))
const ProjectContext = Loadable(lazy(() => import("./context/ProjectContext")))
const ListTask = Loadable(lazy(() => import("./pages/ListTask")))
const TaskUser = Loadable(lazy(() => import("./pages/TaskUser")))
const ManageAccount = Loadable(lazy(() => import("./pages/ACM/ManageAccount")))
const AMC = Loadable(lazy(() => import("./pages/ACM")))
const ListAccount = Loadable(
  lazy(() => import("./pages/ACM/ManageAccount/ListAccount"))
)
const StorageAcm = Loadable(
  lazy(() => import("./pages/ACM/ManageAccount/StorageAcm"))
)
const RevenueExpenditure = Loadable(
  lazy(() => import("./pages/ACM/RevenueExpenditure"))
)
const RequestPaymentAdvance = Loadable(
  lazy(() => import("./pages/ACM/RequestPaymentAdvance"))
)
const NotFound = Loadable(lazy(() => import("./pages/NotFound")))

const RouterConfig = () => {
  // const createRoutes = useRoutes([
  //   {
  //     path: PATH.LOGIN,
  //     element: (
  //       <PublicGuard>
  //         <Login />
  //       </PublicGuard>
  //     )
  //   },
  //   {
  //     path: PATH.HOME,
  //     element: (
  //       <AuthenticatedGuard>
  //         <MainLayout />
  //       </AuthenticatedGuard>
  //     ),
  //     children: [
  //       {
  //         path: PATH.HOME,
  //         element: <Navigate to={PATH.HOME} />
  //       },
  //       {
  //         path: PATH.HOME,
  //         element: <Home />
  //       },
  //       {
  //         path: "projects",
  //         element: <ProjectContext />,
  //         children: [
  //           {
  //             path: "list",
  //             element: <ListTask />
  //           },
  //           {
  //             path: "task-user",
  //             element: <TaskUser />
  //           }
  //         ]
  //       },
  //       {
  //         path: "AMC",
  //         element: <AMC />
  //       }
  //     ]
  //   }
  // ])
  // return createRoutes

  return (
    <BrowserRouter>
      <Routes>
        <Route path={PATH.LOGIN} element={<Login />} />
        <Route
          path="/"
          element={
            <AuthenticatedGuard>
              <MainLayout />
            </AuthenticatedGuard>
          }
        >
          <Route path="/" element={<Home />} />
          <Route path="projects" element={<ProjectContext />}>
            <Route path="list" element={<ListTask />} />
            <Route path="task-user" element={<TaskUser />} />
            <Route path="*" element={<NotFound />} />
          </Route>

          <Route path={PATH.ACM} element={<AMC />}>
            <Route
              path={PATH.ACM}
              element={<Navigate to={PATH.LIST_ACCOUNT} />}
            />
            <Route path={PATH.LIST_ACCOUNT} element={<ListAccount />} />
            <Route path={PATH.STORAGE_ACM} element={<StorageAcm />} />
            <Route
              path={PATH.REVENUE_EXPENDITURE}
              element={<RevenueExpenditure />}
            />
            <Route
              path={PATH.REQUEST_PAYMENT_ADVANCE}
              element={<RequestPaymentAdvance />}
            />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Route>

        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  )
}

export default RouterConfig
