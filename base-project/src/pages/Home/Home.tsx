import { useTranslation } from "react-i18next";

const Home = () => {
  const { t } = useTranslation();

  return <div className='home'>{t("home-layout")}</div>;
};
export default Home;
