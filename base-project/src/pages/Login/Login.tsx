import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useAppDispatch } from "store";
import { PATH } from "constants/paths";
import AuthLayout from "../../layouts/AuthLayout/AuthLayout";
import { doLogin } from "./Login.thunks";
import ToastDemo from "components/Toast/Toast.demo";
import UploadFileDemo from "components/UploadFile/UploadFile.demo";

const Login = () => {
  const { error, loading } = useSelector((state: AppState) => state.auth);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const onSubmit = ({ email, password }) => {
    dispatch(doLogin({ email, password }))
      .unwrap()
      .then(() => {
        navigate(PATH.HOME);
      });
  };

  return (
    <AuthLayout>
      <div className='login'>
        <ToastDemo />
        <UploadFileDemo />
        <p>{t("login-layout")}</p>
      </div>
    </AuthLayout>
  );
};
export default Login;
